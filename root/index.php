<?php

namespace Cygol3;

try {
	require('../includes/pre.php');

	$action = getStringFromRequest('action', 'home');

	switch ($action) {
	case 'home':
	case 'create_account':
	case 'accept':
	case 'login':
	case 'qr':
	case 'recover_account':
	case 'send_recovery_mail':
	case 'about':
	case 'help':
	case 'display_group':
	case 'display_expense':

	case 'create_expense':
	case 'create_group':
	case 'create_participation':
	case 'create_payback':
	case 'remove_expense':
	case 'remove_participation':
	case 'update_expense':
	case 'update_group':

		break;
	default:
		if (Cygol3::$logged_user == NULL) {
			$action = 'home';
		}
		if (!file_exists("../includes/actions/$action.php")) {
			$action = 'home';
		}
		break;
	}

	perform_action($action);
}
catch (\Exception $ex) {
	try {
		set_feedback($ex->getMessage(), "danger");
		error_log("Oops: ".$ex->getMessage());
		export_view_param('logged_user', NULL);
		show_view("oops");
	}
	catch (\Exception $ex2) {
		$msg = _("Ouch, ouch, ouch.  Something went wrong in the code that takes care of things going wrong.  Giving up, sorry.");
		echo $msg;
		echo $ex2->getMessage();

		error_log("Argh: ".$ex2->getMessage());
	}
}
