<?php

namespace Cygol3;

$debug = false;

function require_login() {
	if (!Cygol3::$logged_user) {
		throw new NotAllowedException ('Not logged in');
	}
}

function require_group_access($g) {
	if (!$g->check_access()) {
		throw new NotAllowedException ('No access to that group');
	}
}

try {
	require('../includes/pre.php');

	$json = getStringFromRequest('params');
	$action = getStringFromRequest('action');

	$params = json_decode($json, true);

	switch ($action) {
	case 'create_account':
		$u =& create_account($params);
		$result = array('acct_id' => $u->id,
				'nick' => $u->get_actor()->nick,
				'email' => $u->email,
				'auth_cookie' => $u->auth_cookie);
		break;
	case 'login':
		try {
			$u =& get_account_by_email_pass($params['email'],
							$params['password']);
			$a = $u->get_actor();
			$result = array('status' => 'OK',
					'acct_id' => $u->id,
					'nick' => $a->nick,
					'actor_id' => $a->id,
					'email' => $u->email,
					'auth_cookie' => $u->auth_cookie);
		}
		catch (NotFoundException $ex) {
			$result = array('status' => 'FAIL');
		}
		break;
	case 'create_actor':
		require_login();
		$a =& create_actor($params);
		$result = array('actor_id' => $a->id,
				'nick' => $a->nick);
		break;

	case 'create_group':
		require_login();
		$g =& create_group($params);
		$result = array('group_id' => $g->id,
				'group_name' => $g->name,
				'group_fraction' => $g->fraction,
				'creator_actor_id' => $g->creator->id,
				'creator_actor_nick' => $g->creator->nick);
		break;
	case 'update_group':
		require_login();
		$group =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($group);
        $group->name = $params['name'];
        $group->locale = canonicalize_locale($params['locale'], Cygol3::$config['known_regions']);
        $group->fraction = $params['fraction'];
        $group->save();
		$result = array('group_id' => $group->id);
		break;
	case 'create_expense':
		require_login();
		$g =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($g);
		$expense =& create_expense($params);
		$result = array('expense_id' => $expense->id);
		break;
	case 'my_groups':
		require_login();
		$u =& Cygol3::$logged_user;
		$result = array();
		foreach ($u->get_groups() as $g) {
			$r = array();
			$r['group_id'] = $g->id;
			$r['group_name'] = $g->name;
			$r['group_fraction'] = $g->fraction;
			$r['creator_id'] = $g->creator->id;
			$r['creator_nick'] = $g->creator->nick;
			$result[] = $r;
		}
		break;
	case 'my_friends':
		require_login();
		$u =& Cygol3::$logged_user;
		$result = array();
		foreach ($u->get_recently_active_friends() as $f) {
			$r = array();
			$r['actor_id'] = $f->id;
			$r['actor_nick'] = $f->nick;
			$a = $f->fetch_account();
			if ($a) {
				$r['acct_id'] = $a->id;
			} else {
				$r['acct_id'] = 0;
			}
			$result[] = $r;
		}
		break;
	case 'get_balances':
		require_login();
		$g =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($g);
		$result = array();
		$result['balances'] = array();
		foreach ($g->get_balances() as $b) {
			$r = array();
			$r['actor_id'] = $b['actor']->id;
			$r['actor_nick'] = $b['actor']->nick;
			$r['balance'] = $b['balance'];
			$result['balances'][] = $r;
		}
		foreach ($g->get_suggestions() as $s) {
			$r = array();
			$r['from_id'] = $s['from']->id;
			$r['from_nick'] = $s['from']->nick;
			$r['to_id'] = $s['to']->id;
			$r['to_nick'] = $s['to']->nick;
			$r['amount'] = $s['amount'];
			$result['suggestions'][] = $r;
		}
		break;
	case 'create_payback':
		require_login();
		$g =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($g);
		$expense =& create_expense(array('group_id' => $params['group_id'],
						'is_payback' => 1,
						'payer_actor_id' => $params['from'],
						'amount' => $params['amount'],
						'name' => $params['name'],
						'guests' => array(array('guest_actor_id' => $params['to'],
									'nb_parts' => 1))));
		$result = array('expense_id' => $expense->id);
		break;
	case 'get_expenses':
		require_login();
		$g =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($g);
		$result = array();
		foreach ($g->get_expenses() as $expense) {
			$t = array();
			$t['expense_id'] = $expense->id;
			$t['name'] = $expense->name;
			$t['amount'] = $expense->amount;
			$t['is_payback'] = $expense->is_payback;
			$t['payer_id'] = $expense->payer->id;
			$t['payer_nick'] = $expense->payer->nick;

			$t['participations'] = array();
			foreach ($expense->get_participations() as $participation) {
				$p = array();
				$p['participation_id'] = $participation->id;
				$p['guest_nick'] = $participation->guest->nick;
				$p['guest_id'] = $participation->guest->id;
				$p['parts'] = $participation->parts;

				$t['participations'][] = $p;
			}
			$result[] = $t;
		}
		break;
	case 'get_potential_payers':
		require_login();
		$g =& ObjectCache::retrieve_object('Group', $params['group_id']);
		require_group_access($g);
		$result = array();
		$potential_payers = get_potential_payers($g);
		foreach ($potential_payers as $pp) {
			$t = array();
			$t['guest_nick'] = $pp->nick;
			$t['guest_id'] = $pp->id;
			$result[] = $t;
		}
		break;
	case 'update_participation':
		require_login();
		$expense =& ObjectCache::retrieve_object('Expense', $params['expense_id']);
		require_group_access($expense->group);
		$participation = create_participation($params);
		$result = array('participation_id' => $participation->id);
		break;
	case 'delete_participation':
		require_login();
		$participation =& ObjectCache::retrieve_object('Participation', $params['participation_id']);
		require_group_access($participation->expense->group);
		$participation->delete();
		$result = array();
		break;
	default:
		throw new \Exception(_("No method selected."));
	}

	$res['data'] = $result;
	$res['status'] = 'OK';
}
catch (DBException $ex) {
	$res['status'] = "Database exception: ". $ex->getMessage();
}
catch (\Exception $ex) {
	$res['status'] = "Unknown exception: ". $ex->getMessage();
}

$jresult = json_encode($res);
error_log("$action / $json => $jresult");
print $jresult;
