--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_recovery_codes; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE account_recovery_codes (
    acct_id integer NOT NULL,
    code_hash text DEFAULT ''::text NOT NULL,
    tstamp integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.account_recovery_codes OWNER TO chacunsapart;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE accounts (
    id integer NOT NULL,
    email text DEFAULT ''::text NOT NULL,
    password_hash text DEFAULT ''::text NOT NULL,
    auth_cookie text DEFAULT ''::text NOT NULL,
    actor_id integer DEFAULT 0 NOT NULL,
    locale text DEFAULT 'fr_FR.UTF-8'::text NOT NULL
);


ALTER TABLE public.accounts OWNER TO chacunsapart;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: chacunsapart
--

CREATE SEQUENCE accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_id_seq OWNER TO chacunsapart;

--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chacunsapart
--

ALTER SEQUENCE accounts_id_seq OWNED BY accounts.id;


--
-- Name: actors; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE actors (
    id integer NOT NULL,
    nick text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.actors OWNER TO chacunsapart;

--
-- Name: actors_id_seq; Type: SEQUENCE; Schema: public; Owner: chacunsapart
--

CREATE SEQUENCE actors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.actors_id_seq OWNER TO chacunsapart;

--
-- Name: actors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chacunsapart
--

ALTER SEQUENCE actors_id_seq OWNED BY actors.id;


--
-- Name: expenses; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE expenses (
    id integer NOT NULL,
    group_id integer NOT NULL,
    payer_actor integer NOT NULL,
    amount integer NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    is_payback integer DEFAULT 0 NOT NULL,
    tstamp integer DEFAULT 0 NOT NULL,
    CONSTRAINT transactions_amount_check CHECK ((amount > 0))
);


ALTER TABLE public.expenses OWNER TO chacunsapart;

--
-- Name: expenses_id_seq; Type: SEQUENCE; Schema: public; Owner: chacunsapart
--

CREATE SEQUENCE expenses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expenses_id_seq OWNER TO chacunsapart;

--
-- Name: expenses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chacunsapart
--

ALTER SEQUENCE expenses_id_seq OWNED BY expenses.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    creator_actor integer NOT NULL,
    tstamp integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    invitation_cookie text DEFAULT ''::text NOT NULL,
    locale text DEFAULT 'fr_FR.UTF-8'::text NOT NULL,
    fraction integer DEFAULT 1 NOT NULL,
    CONSTRAINT groups_fraction_check CHECK ((fraction > 0))
);


ALTER TABLE public.groups OWNER TO chacunsapart;

--
-- Name: participations; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE participations (
    id integer NOT NULL,
    expense_id integer NOT NULL,
    guest_actor integer NOT NULL,
    parts integer DEFAULT 1 NOT NULL,
    tstamp integer DEFAULT 0 NOT NULL,
    CONSTRAINT participations_parts_check CHECK ((parts > 0))
);


ALTER TABLE public.participations OWNER TO chacunsapart;

--
-- Name: group_actor_vw; Type: VIEW; Schema: public; Owner: chacunsapart
--

CREATE VIEW group_actor_vw AS
    (SELECT g.id AS group_id, g.name AS group_name, g.tstamp AS group_tstamp, g.invitation_cookie AS group_invitation_cookie, actors.id AS actor_id, actors.nick AS actor_nick, accounts.id AS acct_id, actors.id AS creator_id, actors.nick AS creator_nick FROM ((groups g JOIN actors ON ((g.creator_actor = actors.id))) JOIN accounts ON ((accounts.actor_id = actors.id))) UNION SELECT g.id AS group_id, g.name AS group_name, g.tstamp AS group_tstamp, g.invitation_cookie AS group_invitation_cookie, actors.id AS actor_id, actors.nick AS actor_nick, accounts.id AS acct_id, creator.id AS creator_id, creator.nick AS creator_nick FROM ((((actors creator JOIN groups g ON ((g.creator_actor = creator.id))) JOIN expenses transactions ON ((g.id = transactions.group_id))) JOIN actors ON ((transactions.payer_actor = actors.id))) LEFT JOIN accounts ON ((accounts.actor_id = actors.id)))) UNION SELECT g.id AS group_id, g.name AS group_name, g.tstamp AS group_tstamp, g.invitation_cookie AS group_invitation_cookie, actors.id AS actor_id, actors.nick AS actor_nick, accounts.id AS acct_id, creator.id AS creator_id, creator.nick AS creator_nick FROM (((((actors creator JOIN groups g ON ((g.creator_actor = creator.id))) JOIN expenses transactions ON ((g.id = transactions.group_id))) JOIN participations ON ((transactions.id = participations.expense_id))) JOIN actors ON ((participations.guest_actor = actors.id))) LEFT JOIN accounts ON ((accounts.actor_id = actors.id)));


ALTER TABLE public.group_actor_vw OWNER TO chacunsapart;

--
-- Name: group_participations_vw; Type: VIEW; Schema: public; Owner: chacunsapart
--

CREATE VIEW group_participations_vw AS
    SELECT groups.id AS group_id, expense.id AS expense_id, expense.payer_actor, expense.amount, participation.id AS participation_id, participation.guest_actor, participation.parts FROM ((((groups JOIN expenses expense ON ((groups.id = expense.group_id))) JOIN actors p ON ((expense.payer_actor = p.id))) JOIN participations participation ON ((expense.id = participation.expense_id))) JOIN actors g ON ((participation.guest_actor = g.id)));


ALTER TABLE public.group_participations_vw OWNER TO chacunsapart;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: chacunsapart
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO chacunsapart;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chacunsapart
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: chacunsapart; Tablespace: 
--

CREATE TABLE invitations (
    cookie text NOT NULL,
    actor_id integer NOT NULL,
    invited_by integer NOT NULL,
    tstamp integer NOT NULL
);


ALTER TABLE public.invitations OWNER TO chacunsapart;

--
-- Name: participations_id_seq; Type: SEQUENCE; Schema: public; Owner: chacunsapart
--

CREATE SEQUENCE participations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participations_id_seq OWNER TO chacunsapart;

--
-- Name: participations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chacunsapart
--

ALTER SEQUENCE participations_id_seq OWNED BY participations.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY accounts ALTER COLUMN id SET DEFAULT nextval('accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY actors ALTER COLUMN id SET DEFAULT nextval('actors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY expenses ALTER COLUMN id SET DEFAULT nextval('expenses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY participations ALTER COLUMN id SET DEFAULT nextval('participations_id_seq'::regclass);


--
-- Name: account_recovery_codes_acct_id_key; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY account_recovery_codes
    ADD CONSTRAINT account_recovery_codes_acct_id_key UNIQUE (acct_id);


--
-- Name: accounts_auth_cookie_key; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_auth_cookie_key UNIQUE (auth_cookie);


--
-- Name: accounts_email_key; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_email_key UNIQUE (email);


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: actors_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY actors
    ADD CONSTRAINT actors_pkey PRIMARY KEY (id);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: invitations_actor_id_invited_by_key; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_actor_id_invited_by_key UNIQUE (actor_id, invited_by);


--
-- Name: invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_pkey PRIMARY KEY (cookie);


--
-- Name: participations_expense_id_guest_actor_key; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_expense_id_guest_actor_key UNIQUE (expense_id, guest_actor);


--
-- Name: participations_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_pkey PRIMARY KEY (id);


--
-- Name: transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: chacunsapart; Tablespace: 
--

ALTER TABLE ONLY expenses
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: account_recovery_codes_acct_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY account_recovery_codes
    ADD CONSTRAINT account_recovery_codes_acct_id_fkey FOREIGN KEY (acct_id) REFERENCES accounts(id);


--
-- Name: accounts_actor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES actors(id);


--
-- Name: expenses_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY expenses
    ADD CONSTRAINT expenses_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: groups_creator_actor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_creator_actor_fkey FOREIGN KEY (creator_actor) REFERENCES actors(id);


--
-- Name: invitations_actor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES actors(id);


--
-- Name: invitations_invited_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_invited_by_fkey FOREIGN KEY (invited_by) REFERENCES accounts(id);


--
-- Name: participations_expense_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_expense_id_fkey FOREIGN KEY (expense_id) REFERENCES expenses(id);


--
-- Name: participations_guest_actor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_guest_actor_fkey FOREIGN KEY (guest_actor) REFERENCES actors(id);


--
-- Name: transactions_payer_actor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: chacunsapart
--

ALTER TABLE ONLY expenses
    ADD CONSTRAINT transactions_payer_actor_fkey FOREIGN KEY (payer_actor) REFERENCES actors(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: account_recovery_codes; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE account_recovery_codes FROM PUBLIC;
REVOKE ALL ON TABLE account_recovery_codes FROM chacunsapart;
GRANT ALL ON TABLE account_recovery_codes TO chacunsapart;
GRANT SELECT ON TABLE account_recovery_codes TO zabbix;


--
-- Name: accounts; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE accounts FROM PUBLIC;
REVOKE ALL ON TABLE accounts FROM chacunsapart;
GRANT ALL ON TABLE accounts TO chacunsapart;
GRANT SELECT ON TABLE accounts TO zabbix;


--
-- Name: actors; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE actors FROM PUBLIC;
REVOKE ALL ON TABLE actors FROM chacunsapart;
GRANT ALL ON TABLE actors TO chacunsapart;
GRANT SELECT ON TABLE actors TO zabbix;


--
-- Name: expenses; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE expenses FROM PUBLIC;
REVOKE ALL ON TABLE expenses FROM chacunsapart;
GRANT ALL ON TABLE expenses TO chacunsapart;
GRANT SELECT ON TABLE expenses TO zabbix;


--
-- Name: groups; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE groups FROM PUBLIC;
REVOKE ALL ON TABLE groups FROM chacunsapart;
GRANT ALL ON TABLE groups TO chacunsapart;
GRANT SELECT ON TABLE groups TO zabbix;


--
-- Name: participations; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE participations FROM PUBLIC;
REVOKE ALL ON TABLE participations FROM chacunsapart;
GRANT ALL ON TABLE participations TO chacunsapart;
GRANT SELECT ON TABLE participations TO zabbix;


--
-- Name: invitations; Type: ACL; Schema: public; Owner: chacunsapart
--

REVOKE ALL ON TABLE invitations FROM PUBLIC;
REVOKE ALL ON TABLE invitations FROM chacunsapart;
GRANT ALL ON TABLE invitations TO chacunsapart;
GRANT SELECT ON TABLE invitations TO zabbix;


--
-- PostgreSQL database dump complete
--

