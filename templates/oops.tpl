{include file="../templates/header.tpl" title="{t}Oops{/t}"}

{include '../templates/function__crumbs.tpl'}

<div class="hero-unit">
<h1>{t}Oops.{/t}</h1>

<p>{t}Something went wrong.{/t}
</div>

{include '../templates/function__feedback.tpl'}

<p>{t escape=no sn="$sn_styled" ce="$contactemail"}Something went wrong with %1.  I keep an eye on the site so I should already be aware of it, but if the problem persists please send me an email at <a href="mailto:%2">%2</a>.{/t}</p>

{include file="../templates/footer.tpl"}
