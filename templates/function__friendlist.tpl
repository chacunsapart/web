<table class="table"><tbody>
{$seen = 0}
{foreach $friends_groups as $fe}
{$f = $fe['friend']}
{if $f->account && $f->account->id == $logged_user->id}{continue}{/if}
{$seen = $seen + 1}
{if $limit > 0 && $seen > $limit}{break}{/if}
{if $f->account && $f->account->id > 0}
<tr><td colspan="2">
{$f->nick|escape}
{$groups = $fe['groups']}
({foreach $groups as $e}{if $e@iteration > 1}, {/if}
{if $grouplimit > 0 && $e@iteration > $grouplimit}…{break}{else}{$e->name}{/if}
{/foreach})
{else}
<tr><td>
{$f->nick|escape}
{$groups = $fe['groups']}
({foreach $groups as $e}{if $e@iteration > 1}, {/if}
{if $grouplimit > 0 && $e@iteration > $grouplimit}…{break}{else}{$e->name}{/if}
{/foreach})
</td><td>
<form method="{$form_method}" action="{$prefix}invite">
<input type="hidden" name="actor_id" value="{$f->id}" />
<button class="btn btn-primary" type="submit">{t}Invite{/t}</button>
</form>
{/if}
</td><td>
</td></tr>
{/foreach}
</tbody></table>
