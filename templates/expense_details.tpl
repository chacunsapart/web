{include file="../templates/header.tpl" title="{$expense->group->name|escape} / {$expense->name|escape}"}

{$crumbs[] = [$expense->group->name|escape, [ action => 'display_group', 'group_id' => $expense->group->id ]]}
{$crumbs[] = [$expense->name|escape, [ action => 'display_expense', 'expense_id' => $expense->id ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{$expense->name|escape}</h1>

<div class="tab-content">

{$pn = "{$expense->payer->nick|escape}"}
{$am = "{$expense->amount|@format_amount}"}
<div id="expense-summary" class="collapse in">{t escape=no amount=$am payer_nick=$pn}An expense of %1, paid by %2.{/t}
{if $total_parts > 0}
{t escape=no plural="%1 parts in total, for a part value of %2." count=$total_parts/$expense->group->fraction tp=$total_parts|@format_part pv=$part_value|@format_amount}%1 part in total, for a part value of %2.{/t}
{/if}</div>

<div id="modify-expense-button" class="collapse in">
<button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#modify-expense-form" onclick="$('#modify-expense-button').removeClass('in');$('#expense-summary').removeClass('in');">{t}Edit{/t}</button>
</div>

<div class="collapse" id="modify-expense-form">
<h2>{t}Edit this expense{/t}</h2>

<form method="{$form_method}" action="{$prefix}update_expense" class="form-horizontal">
<input type="hidden" name="group_id" value="{$expense->group->id}" />
<input type="hidden" name="expense_id" value="{$expense->id}" />
<div class="control-group">
<label class="control-label" for="name">{t}Description:{/t}</label>
<div class="controls">
<input name="name" type="text" value="{$expense->name|escape}" />
</div></div>

<div class="control-group">
<label class="control-label" for="amount">{t}Amount:{/t}</label>
<div class="controls">
<input name="amount" type="number" min="{$expense->group->cent}" step="{$expense->group->cent}" value="{$expense->amount|@format_amount_form}" />
</div></div>

<div class="control-group">
<label class="control-label" for="payer_id">{t}Payer:{/t}</label>
<div class="controls">
<select name="payer_id" onchange="if (this.value == 0) { $('#new_payer_input').addClass('in');} else { $('#new_payer_input').removeClass('in');}">
{foreach $potential_payers as $f}
<option value="{$f->id}" {if $f->id == $expense->payer->id}selected="selected"{/if}>{$f->nick|escape}</option>
{/foreach}
<option value="0">{t}New…{/t}</option>
</select>
</div></div>

<div class="control-group collapse" id="new_payer_input">
<label class="control-label" for="new_actor_nick" >{t}Name:{/t}</label>
<div class="controls">
<input name="new_actor_nick" type="text" placeholder="{t}New guest{/t}" />
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Update{/t}</button>
<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#modify-expense-form" onclick="$('#modify-expense-button').addClass('in');$('#expense-summary').addClass('in');">{t}Cancel{/t}</button>
</div></div>
</form>

<h2>{t}Delete this expense{/t}</h2>

<form method="{$form_method}" action="{$prefix}remove_expense" class="form-horizontal">
<input type="hidden" name="expense_id" value="{$expense->id}" />
{if $total_parts == 0}
<input type="hidden" name="sure" value="1" />
{else}
<div class="control-group collapse in" id="#new_actor_input">
<label class="control-label" for="sure" >{t}Sure?{/t}</label>
<div class="controls">
<input type="checkbox" name="sure" value="1" />
</div></div>
{/if}
<div class="control-group">
<div class="controls">
<button class="btn btn-warning" type="submit">{t}Remove{/t}</button>
<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#modify-expense-form" onclick="$('#modify-expense-button').addClass('in');$('#expense-summary').addClass('in');">{t}Cancel{/t}</button>
</div></div>
</form>

</div>


{if $participants|@count > 0}
<h2>{t}Guests{/t}</h2>

<table class="table">
<thead>
<tr>
<th>{t}Guest{/t}</th>
<th>{t}Parts{/t}</th>
<th>{t}Options{/t}</th>
</tr>
</thead>
<tbody>
{foreach $participants as $participant}
<tr>
<td>{$participant['guest']->nick|escape}</td>
<td><span class="label {if $participant['parts'] > 0}label-success{else}label-default{/if}">{$participant['parts']|format_part}</span></td>
<td>

<form method="{$form_method}" action="{$prefix}create_participation" class="inline">
<input type="hidden" name="expense_id" value="{$expense->id}" />
<input type="hidden" name="guest_id" value="{$participant['guest']->id}" />
<input type="hidden" name="parts" value="+{$expense->group->fraction}" />
{if $participant['parts'] > 0}
<button class="btn btn-primary" type="submit">+1</button>
{else}
<button class="btn btn-primary" type="submit">{t}Add{/t}</button>
{/if}
</form>

{if $expense->group->fraction > 1 && $participant['parts'] > 0}

<form method="{$form_method}" action="{$prefix}create_participation" class="inline">
<input type="hidden" name="expense_id" value="{$expense->id}" />
<input type="hidden" name="guest_id" value="{$participant['guest']->id}" />
<input type="hidden" name="parts" value="+1" />
<button class="btn btn-primary btn-xs" type="submit">+{1|format_part}</button>
</form>

{if $participant['parts'] > 1}
<form method="{$form_method}" action="{$prefix}create_participation" class="inline">
<input type="hidden" name="expense_id" value="{$expense->id}" />
<input type="hidden" name="guest_id" value="{$participant['guest']->id}" />
<input type="hidden" name="parts" value="-1" />
<button class="btn btn-primary btn-xs" type="submit">-{1|format_part}</button>
</form>
{/if}

{/if}

{if $participant['parts'] > $expense->group->fraction}
<form method="{$form_method}" action="{$prefix}create_participation" class="inline">
<input type="hidden" name="expense_id" value="{$expense->id}" />
<input type="hidden" name="guest_id" value="{$participant['guest']->id}" />
<input type="hidden" name="parts" value="-{$expense->group->fraction}" />
<button class="btn btn-primary" type="submit">-1</button>
</form>
{elseif $participant['parts'] > 0}
<form method="{$form_method}" action="{$prefix}remove_participation" class="inline">
<input type="hidden" name="expense_id" value="{$expense->id}" />
<input type="hidden" name="participation_id" value="{$participant['px_id']}" />
<input type="hidden" name="group_id" value="{$expense->group->id}" />
<button class="btn btn-warning" type="submit">{t}Remove{/t}</button>
</form>
{/if}
</td>
</tr>
{/foreach}

</tbody>
</table>
{/if}


<div id="create-newguest-button" class="collapse in">
<button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#create-newguest-form" onclick="$('#create-newguest-button').removeClass('in');">{t}Add a new guest{/t}</button>
</div>

<div class="collapse" id="create-newguest-form">
<h2>{t}Add a new guest{/t}</h2>

<form method="{$form_method}" action="{$prefix}create_participation" class="form-horizontal">
<input type="hidden" name="expense_id" value="{$expense->id}" />

{$seen_friend = 0}
{if $other_friends|@count > 0}
<div class="control-group">
<label class="control-label" for="guest_id">{t}Guest:{/t}</label>
<div class="controls">
<select name="guest_id" onchange="if (this.value == 0) { $('#new_actor_input').addClass('in');} else { $('#new_actor_input').removeClass('in');}">
{foreach $other_friends as $f}
{$seen_friend = 1}
<option value="{$f->id}">{$f->nick|escape}</option>
{/foreach}
<option value="0">{t}New…{/t}</option>
</select>
</div></div>
{else}
<input type="hidden" name="guest_id" value="0" />
{/if}

<div class="control-group collapse {if !$seen_friend}in{/if}" id="new_actor_input">
<label class="control-label" for="new_actor_nick">{t}Name:{/t}</label>
<div class="controls">
<input name="new_actor_nick" type="text" placeholder="{t}New guest{/t}" />
</div></div>

<input type="hidden" name="parts" value="{$expense->group->fraction}" />

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Add guest{/t}</button>
</div></div>
</form>
</div>

<a href="{$prefix}display_group?group_id={$expense->group->id}"><button class="btn btn-primary" type="submit">{t gn=$expense->group->name}Done with this expense{/t}</button></a>

<div id="create-clonedexpense-button" class="collapse in pull-right">
<button class="btn btn-info btn-xs" type="button" data-toggle="collapse" data-target="#create-clonedexpense-form" onclick="$('#create-clonedexpense-button').removeClass('in');">{t}Add A Similar Expense{/t}</button>
</div>

<div class="collapse" id="create-clonedexpense-form">
<h2>{t}Add A Similar Expense{/t}</h2>

<form method="{$form_method}" action="{$prefix}create_expense" class="form-horizontal">
<input type="hidden" name="group_id" value="{$expense->group->id}" />
<input type="hidden" name="cloned_expense_id" value="{$expense->id}" />
<div class="control-group">
<label class="control-label" for="name">{t}Description:{/t}</label>
<div class="controls">
<input name="name" type="text" value="{$expense->name}" />
</div></div>

<div class="control-group">
<label class="control-label" for="amount">{t}Amount:{/t}</label>
<div class="controls">
<input name="amount" type="number" min="{$expense->group->cent}" step="{$expense->group->cent}" value="{$expense->amount}" />
</div></div>

<div class="control-group">
<label class="control-label" for="payer_id">{t}Payer:{/t}</label>
<div class="controls">
<select name="payer_id" onchange="if (this.value == 0) { $('#new_actor_input').addClass('in');} else { $('#new_actor_input').removeClass('in');}">
{foreach $potential_payers as $f}
  <option value="{$f->id}" {if $f->id == $expense->payer->id}selected="selected"{/if}>{$f->nick|escape}</option>
{/foreach}
<option value="0">{t}New…{/t}</option>
</select>
</div></div>

<div class="control-group collapse" id="new_actor_input">
<label class="control-label" for="new_actor_nick" >{t}Name:{/t}</label>
<div class="controls">
<input name="new_actor_nick" type="text" placeholder="{t}New guest{/t}" />
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Add Expense{/t}</button>
</div></div>
</form>
</div>

</div>

{include file="../templates/footer.tpl"}
