{include file="../templates/header.tpl" title="{t}Account Recovery{/t}"}

{capture assign=t}
{t}Account Recovery{/t}
{/capture}
{$crumbs[] = [$t, [ action => 'lost_password' ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t}Account Recovery{/t}</h1>

<form method="{$form_method}" action="{$prefix}recover_account" class="form-horizontal">
    <div class="control-group">
    <label class="control-label" for="email">{t}Email:{/t}</label>
    <div class="controls">
    <input name="email" type="email" value="{$email|escape}" placeholder="{t}Email{/t}" />
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="recoverycode">{t}Recovery code:{/t}</label>
    <div class="controls">
    <input name="recoverycode" type="text" value="{$recoverycode|escape}" placeholder="{t}Recovery code{/t}" />
    </div></div>

    <div class="control-group">
    <label class="control-label" for="newpassword1">{t}New Password:{/t}</label>
    <div class="controls">
    <input name="newpassword1" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="newpassword2">{t}Confirm New Password:{/t}</label>
    <div class="controls">
    <input name="newpassword2" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    
    <div class="control-group">
    <div class="controls">
    <button class="btn btn-primary" type="submit">{t}Recover Account{/t}</button>
    </div></div>
</form>

{include file="../templates/footer.tpl"}
