{include file="../templates/header.tpl" title="{t}Accept invitation?{/t}"}

{$crumbs[] = ["{t}Accept invitation?{/t}", [ action => 'accept', c => $invitation->cookie|escape ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t}Accept invitation?{/t}</h1>
<p>{t n=$inviter->nick}Do you want to accept the invitation from %1?{/t}</p>

<h2>{t}Got an account already?{/t}</h2>

<form method="{$form_method}" action="{$prefix}login">
<input type="hidden" name="accept_invitation_cookie" value="{$invitation->cookie|escape}" />
<p>{t}Email:{/t} <input name="email" type="email" /></p>
<p>{t}Password:{/t} <input name="password" type="password" /></p>
<p><button class="btn btn-primary" type="submit">{t}Log in{/t}</button></p>
</form>

<!--
<h2>{t}Oh.  Want to see what it's about?{/t}</h2>

<p>{t escape=no url="demo.php" sn=$sn_styled}Check out the <a href="%1">demo</a> to see what %2 is about.{/t}</p>
-->

<h2>{t}Liked what you saw?  Want an account now?{/t}</h2>

<p>
<form method="{$form_method}" action="{$prefix}create_account">
<input type="hidden" name="accept_invitation_cookie" value="{$invitation->cookie|escape}" />
<p>{t}Name:{/t} <input name="nick" type="text" value="{$invitation->actor->nick|escape}"/></p>
<p>{t}Email:{/t} <input name="email" type="email" /></p>
<p>{t}Password:{/t} <input name="password" type="password" /></p>
<p><button class="btn btn-primary" type="submit">{t}Create account{/t}</button></p>
</form>
</p>

<h2>{t}Oh.  Maybe you'd like to decline the invitation then?{/t}</h2>
<p>
<form method="{$form_method}" action="{$prefix}accept">
<input type="hidden" name="c" value="{$invitation->cookie|escape}" />
<input type="hidden" name="no" value="1" />
<button class="btn btn-warning" type="submit">{t}Decline invitation{/t}</button>
</form>
</p>

{include file="../templates/footer.tpl"}
