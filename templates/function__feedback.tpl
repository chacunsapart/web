{if $feedback != ''}
<div class="alert {$feedback_level}">
<button type="button" class="close" data-dismiss="alert">&times;</button>
{$feedback|escape}
</div>
{/if}
