{include file="../templates/header.tpl" title="{t}Invite a Friend{/t}"}

{$crumbs[] = [$from_group->name|escape, [ action => 'display_group', 'group_id' => $from_group->id ]]}
{$crumbs[] = [$invitation->actor->nick|escape, [ action => 'invite', 'actor_id' => $invitation->actor->id ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t nick=$invitation->actor->nick}Invite %1?{/t}</h1>

<div>
{t escape=no nick=$invitation->actor->nick|escape sn="$sn_styled"}To invite %1 to join %2, send them the following link:{/t}
<a href="{$prefix}accept?c={$invitation->cookie}">{$sn_styled}</a>
</div>
<div>
{t}You can also tell them to flash this QR-code:{/t}
</div>
{include '../templates/function__qr.tpl' p="accept?c={$invitation->cookie}"}

<a href="{$prefix}display_group?group_id={$from_group->id}&show_balances=1"><button class="btn btn-primary" type="submit">{t gn=$from_group->name}Back to %1{/t}</button></a>


{include file="../templates/footer.tpl"}
