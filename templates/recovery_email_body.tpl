{$u = "{$prefix}recover_account?email={$email|escape:'url'}&recoverycode=$recoverycode"}
{t escape=no sn=$sn u=$u n=$name e=$email c=$recoverycode}Dear %3,

Someone (probably you) is trying to recover the account associated
with the "%4" email address at %1.

If it is you, then please use the following recovery code on the
account recovery form:

  %5

Alternatively, you can also use the following direct link:

  %2

You'll be asked to type in a new password for your account, and will
then be able to access %1 again.

Enjoy!
{/t}