{include file="../templates/header.tpl" title="{t}Accept invitation?{/t}"}

{$crumbs[] = ["{t}Accept invitation?{/t}", [ action => 'accept', c => $invitation->cookie ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t}Accept invitation?{/t}</h1>

<p>{t n=$inviter->nick}Do you want to accept the invitation from %1?{/t}</p>

{if $invitation->invited_by->id == $logged_user->id}
<p><span class="badge badge-warning">{t}Warning{/t}</span> {t}This seems to be an invitation you made yourself; make sure you really want to accept it…{/t}</p>
{/if}
<p>
<form method="{$form_method}" action="{$prefix}accept">
<input type="hidden" name="c" value="{$invitation->cookie|escape}" />
<input type="hidden" name="yes" value="1" />
<button class="btn btn-primary" type="submit">{t}Accept invitation{/t}</button>
</form>

<form method="{$form_method}" action="{$prefix}accept">
<input type="hidden" name="c" value="{$invitation->cookie|escape}" />
<input type="hidden" name="no" value="1" />
<button class="btn btn-warning" type="submit">{t}Decline invitation{/t}</button>
</form>
</p>

{include file="../templates/footer.tpl"}
