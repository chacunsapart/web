<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$sn}{if $title != ''} / {$title}{/if}</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/{$stylesheet}" rel="stylesheet">
<link rel="icon" type="image/png" href="logo.png" />
<link rel="apple-touch-icon" href="logo.png" />

</head>
<body>
<script language="javascript" type="text/javascript" src="/javascript/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

<div class="container">
