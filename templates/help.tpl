{include file="../templates/header.tpl" title="{t}Help{/t}"}

{$crumbs[] = ["{t sn=$sn}%1 Help{/t}", [ action => 'help' ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t escape=no sn="$sn_styled"}%1 Help{/t}</h1>

<p>{t escape=no sn="$sn_styled"}While %1 was written with ease of use in mind, there's no harm in having a more detailed manual.{/t}</p>

<h2>{t}Terminology{/t}</h2>

<ul>
<li>{t escape=no}A <em>group</em> is a set of related things.  People, expenses, paybacks, and so on.{/t}</li>
<li>{t escape=no}An <em>expense</em> is something involving money that happened in that group.  It's paid <em>by</em> one person (the “payer”), <em>for</em> one or more persons (the “guests”).  Note that the payer can be one of the guests, but that is in no way compulsory.{/t}</li>
<li>{t escape=no}Within an expense, <em>guests</em> can use one or more <em>parts</em>, if the expense is not to be split equally between guests.{/t}</li>
<li>{t escape=no}All the persons who are involved with a group (either as payer or guest) have a <em>balance</em> within that group; it's the difference between what they've actually paid and what they should have paid if they paid exactly their share.{/t}</li>
<li>{t escape=no}A <em>payback</em> is a transfer of money between two persons within a group, meant to bring their respective balances closer to zero.{/t}</li>
</ul>

<h2>{t}Basic Usage{/t}</h2>

<p>{t escape=no sn="$sn_styled"}The most common way to use %1 is simple:{/t}</p>
<ul>
<li>{t escape=no sn="$sn_styled"}If you have an account at %1 already, log in; otherwise, create one.{/t}</li>
<li>{t escape=no}Create a group from your home page; you'll need to give it a name.{/t}</li>
<li>{t escape=no}Create an expense in that group from the group details page; you'll be asked to give a short description, an amount, and to select who paid for it (you can pick the payer amongst your known friends, or type the name of a new person).{/t}</li>
<li>{t escape=no}Fill in the guests for that expense (again, you can pick guests amongst your known friends or register a new one).  If some guests count for different shares of the expense, adjust the number of parts accordingly (with the +1/-1 buttons).{/t}</li>
<li>{t escape=no}Chances are, you have several expenses to account for; return to the group, and repeat the process to create more expenses.{/t}</li>
<li>{t escape=no}At some point, you'll want to check the balances.  In the group details page, switch to the “balances” tab.  You'll see a list of computed balances, and suggestions for paybacks.{/t}</li>
<li>{t escape=no}To register paybacks, either proceed with the suggestions, or register manual paybacks with the form.{/t}</li>
<li>{t escape=no}If the group goes on, you can register further expenses and paybacks.  The balances and suggestions are updated automatically.{/t}</li>
</ul>

<h2>{t}Friends{/t}</h2>

<p>{t escape=no sn="$sn_styled"}%1 is meant to be usable by groups of friends.  In this context, a friend is someone with whom you share some involvement within a group (either as the creator of the group, the payer for one of its expenses, or one of the guests).{/t}</p>

<p>{t escape=no sn="$sn_styled"}As we've seen, “friends” are registered automatically when you select “New…” when creating an expense or adding a guest to it.  They'll be known to %1, and you can select them again for future expenses.  At this point, you may want to “invite” them from the “balances” tab.  The invitation system generates a link that you can send to your actual friend; if they click on the link (or flash the QR-code), they'll be able to associate their account with that registered “friend”, which will give them access to the groups you have in common.{/t}</p>

<h2>{t}Links To Groups{/t}</h2>

<p>{t escape=no sn="$sn_styled"}The group details pages on %1 also contain a link (and a QR-code) that allows for temporary access to a group.  Be careful with that link: any one who gets it can access and modify the group and its expenses.{/t}</p>

<h2>{t}Languages And Locations{/t}</h2>

<p>{t escape=no sn="$sn_styled"}%1 knows of several languages and locations, which has two consequences.  First, as a registered user, you can pick your favorite display language.  Second, a group is associated with a given location, which allows for groups in different currencies; this alters the way amounts are displayed, but also the way they are stored within %1 and computed.  Be careful if you decide to change the location for a group after you've registered expenses and/or paybacks.{/t}</p>

<h2>{t}Fractional Part Numbers{/t}</h2>

<p>{t escape=no sn="$sn_styled"}Because some groups may have special conventions (such as “half rate for kids“), you can set the numbers of parts of a group to be fractional (by halves, thirds or quarters).  Switching from one setting to another after expenses are registered will change the number of known parts in the simplest possible way while preserving existing ratios (and hence, balances).{/t}</p>

<h2>{t}A Note On Security{/t}</h2>

<p>{t escape=no sn="$sn_styled"}%1 is meant to track expenses for groups of friends.  It is expected that the persons involved act in good faith.  If you share a group with someone, they'll be able to see and change the expenses and paybacks.  This makes it easy to keep tabs as the holidays/parties/regular pub meetings go on, since everyone can register their own expense as soon as they make them, but there's no protection against malicious “friends”.{/t}</p>

<p>{t escape=no sn="$sn_styled"}Note that a group (and its expenses/paybacks) is only accessible to people involved with it, so complete strangers shouldn't be able to come and disrupt your accounts if you haven't shared the group link.{/t}</p>

<p>{t escape=no sn="$sn_styled"}Within a group of well-meaning friends, however, %1 is expected to be convenient and simple to use.  I hope it fulfills that goal!{/t}</p>

{include file="../templates/footer.tpl"}
