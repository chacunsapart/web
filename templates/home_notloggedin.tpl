{include file="../templates/header.tpl" title=""}

{include '../templates/function__crumbs.tpl'}

<div class="jumbotron" style="background: url('/logo-clear.png') no-repeat center">
<h1>{$sn}</h1>

<p>{t}Keep shared expenses simple.{/t}
</div>

{include '../templates/function__feedback.tpl'}

    <div class="row">
        <div class="col-md-5">
          <h2>{t}Keep track of your shared expenses{/t}</h2>
          <p>{t escape=no sn=$sn_styled}%1 is about managing shared expenses amongst a group of friends.  You have holidays together, and you all pay bits of them along the way; house rental, gas for the trip, groceries, drinks, sun lotion… %1 helps reconciling these accounts.  It keeps track of the expenses, and calculates who owes how much to whom.{/t}</p>
        </div>

        <div class="col-md-4">

{if $groups|@count == 0}
<div class="collapse in" id="create-group-form">
{else}
          <h2>{t}My Recent Groups{/t}</h2>

<div class="container-fluid">
<div class="panel-group" id="groups_accordion">
{$seen = 0}
{foreach $groups_participants as $gp}
{$seen = $seen + 1}
{$e = $gp['group']}
{$p = $gp['participants']}
{$tx = $gp['expenses']}
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#groups_accordion" href="#collapse_group_{$seen}">
{$e->name|escape}
        </a>
      </h4>
    </div>
    <div id="collapse_group_{$seen}" class="panel-collapse collapse {if $seen == 1}in{/if}">
      <div class="panel-body">
<div>{t}Participants:{/t}
{foreach $p as $a}
  {$a->nick}{if !$a@last}, {/if}
{/foreach}
</div><div>
{foreach $tx as $t}
{if $t->is_payback}{continue}{/if}
{t tn=$t->name}Last expense: %1{/t}
{break}
{/foreach}
</div>
<div><a href="{$prefix}display_group?group_id={$e->id}"><button class="btn btn-primary center-block" type="submit">{t}Details{/t}</button></a></div>

    </div>
  </div>
  </div>

{/foreach}
    </div>
  </div>

<div id="create-group-button" class="collapse in">
<button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#create-group-form" onclick="$('#create-group-button').removeClass('in');">{t}Create A Group{/t}</button>
</div>

<div class="collapse" id="create-group-form">
{/if}
<h2>{t}Create A Group{/t}</h2>
          <p>{t escape=no sn=$sn_styled}You can start using %1 straight away! Just give a name to your group, and you'll be on your way.{/t}</p>

<form method="{$form_method}" action="{$prefix}create_group" class="form-inline">
<div class="control-group">
<label class="control-label" for="name">{t}Group Name:{/t}</label>
<div class="controls">
<input name="name" type="text" placeholder="{t}Group Name{/t}"/>
</div></div>

<div class="control-group">
<label class="control-label" for="actor_name">{t}Your Name:{/t}</label>
<div class="controls">
<input name="creator_name" type="text" placeholder="{t}Your Name{/t}"/>
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Create Group{/t}</button>
</div></div>
</form>
</div>
        </div>

        <div class="col-md-3">
	<h2>{t}Log In{/t}</h2>
          <p>{t escape=no sn=$sn_styled}If you want to come back later and update groups and expenses, you should register an account at %1.{/t}</p>

<div class="collapse in" id="login-block">
<form method="{$form_method}" action="{$prefix}login" class="form-signin">
<input name="email" type="email" placeholder="{t}Email{/t}"/>
<input name="password" type="password" placeholder="{t}Password{/t}"/>
<button class="btn btn-large btn-primary" type="submit">{t}Log in{/t}</button>
</form>

<div id="lost-pw-button" class="collapse in">
<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#lost-pw-form" onclick="$('#lost-pw-button').removeClass('in');$('#login-block').removeClass('in');$('#create-acct-button').removeClass('in')">{t}Lost Your Password?{/t}</button>
</div>
</div>

<div class="collapse" id="lost-pw-form">
<h2>{t}Lost Your Password?{/t}</h2>

<p>{t}Type your email address below; we'll send an email so that you can recover your account.{/t}</p>

<form method="{$form_method}" action="{$prefix}send_recovery_mail" class="form-inline">
<div class="control-group">
<div class="controls">
<input name="email" type="email" placeholder="{t}Email{/t}"/>
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary btn-large" type="submit">{t}Send A Recovery Email{/t}</button>
</div></div>
</form>
</div>

<div id="create-acct-button" class="collapse in">
<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#create-acct-form" onclick="$('#login-block').removeClass('in');$('#create-acct-button').removeClass('in');">{t}Create An Account{/t}</button>
</div>

<div class="collapse" id="create-acct-form">
<h2>{t}Create Your Account{/t}</h2>
<form method="{$form_method}" action="{$prefix}create_account" class="form-signin">

<div class="control-group">
<label class="control-label" for="nick">{t}Name:{/t}</label>
<div class="controls">
<input name="nick" type="text" placeholder="{t}Name{/t}"/>
</div></div>

<div class="control-group">
<label class="control-label" for="email">{t}Email:{/t}</label>
<div class="controls">
<input name="email" type="email" placeholder="{t}Email{/t}"/>
</div></div>

<div class="control-group">
<label class="control-label" for="password">{t}Password:{/t}</label>
<div class="controls">
<input name="password" type="password" placeholder="{t}Password{/t}"/>
</div></div>

<button class="btn btn-large btn-primary" type="submit">{t}Create account{/t}</button>
</form>
</div>
        </div>
      </div>

{include file="../templates/footer.tpl"}
