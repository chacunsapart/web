{include file="../templates/header.tpl" title="{t s=$sn}About %1{/t}"}

{$crumbs[] = ["{t escape=no sn=$sn_styled}About %1{/t}", [ action => 'about' ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t escape=no sn="$sn_styled"}About %1{/t}</h1>

<p>{t escape=no sn="$sn_styled"}%1 is about managing shared expenses amongst a group of friends.  You have holidays together, and you all pay bits of them along the way; house rental, gas for the trip, groceries, drinks, sun lotion… %1 helps reconciling these accounts.  It keeps track of the expenses, and calculates who owes how much to whom.{/t}</p>

<p>{t escape=no sn="$sn_styled"}%1 is a service written for fun by <a href="http://roland.entierement.nu/">Roland Mas</a> (and I also host the instance at <a href="https://chacunsapart.com/"><tt>chacunsapart.com</tt></a>, in case you're somewhere else).  While I take pride in making things that work correctly, there's absolutely no guarantee or anything.{/t}</p>

<p>{t escape=no ce="$contactemail"}Should you feel the need to contact me, please send me an email at <a href="mailto:%1">%1</a>.{/t}</p>

<p>{t escape=no sn="$sn_styled" ex=$statistics['expenses'] gr=$statistics['groups'] ac=$statistics['actors']}For those who care about numbers: %1 currently knows about %2 expenses in %3 groups, involving %4 persons in total.{/t}</p>

{include '../templates/function__qr.tpl' p=''}

{include file="../templates/footer.tpl"}
