<ul class="breadcrumb row">
{foreach $crumbs as $c}
  <li>
  {$args = $c[1]}
  <a href="{$prefix}{$args['action']}{$seen = 0}{foreach $args as $k => $v}{if $k == 'action'}{continue}{/if}{if $seen == 0}{$seen = 1}?{/if}{$k}={$v}{/foreach}">{if $c@first}<img src="/logo-small.png" /> {/if}{$c[0]}</a>
  </li>
{/foreach}
{if $logged_user}
  <div class="btn-group pull-right"> <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">{$logged_name|escape} <span class="caret"></span></a>
  <ul class="dropdown-menu">
  <li><a href="{$prefix}my_account">{t}My Account{/t}</a></li>
  <li><a href="{$prefix}logout">{t}Log out{/t}</a></li>
  </ul>
  </div>
{else}
  <div class="btn-group pull-right"> <a class="btn dropdown-toggle btn-mini btn-info" data-toggle="dropdown" href="#" onclick="$('.dropdown-menu input').click(function(e) { e.stopPropagation(); } );">{t}Log In{/t} <span class="caret"></span></a>
  <ul class="dropdown-menu">
  <li class="dropdown">
  <div style="padding: 1em">
  <form method="{$form_method}" action="{$prefix}login">
  <input name="email" type="email" placeholder="{t}Email{/t}">
  <input name="password" type="password" placeholder="{t}Password{/t}"/>
  <button class="btn btn-primary" type="submit">{t}Log in{/t}</button>
  </form>
  </div>
  </li>
  </ul>
  </div>

  <div class="btn-group pull-right"> <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">{t}Language{/t} <span class="caret"></span></a>
  <ul class="dropdown-menu">
  {foreach $main_locales as $l}<li><a href="{$prefix}{$args['action']}{$seen = 0}{foreach $args as $k => $v}{if $k == 'action'}{continue}{/if}{if $seen == 0}{$seen = 1}?{/if}{$k}={$v}{/foreach}{if $seen == 0}?{else}&{/if}set_default_locale={$l['locale']}">{$l['display_name']}</a></li>{/foreach}
  </ul>
  </div>

{/if}
</ul>
