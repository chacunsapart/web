{include file="../templates/header.tpl" title=""}

{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<div class="jumbotron" style="background: url('/logo-clear.png') no-repeat center">
<h1>{$sn}</h1>
</div>

    <div class="row">
        <div class="col-md-8">
{if $groups_participants|@count == 0}
<h2>{t}No Groups?{/t}</h2>
<div>{t}Your groups will be listed here as soon as you register them (or get invited to them).{/t}</div>
{else}
          <h2>{t}My Groups{/t}</h2>

{$min_display = 10}{$max_age = 30}
<div class="container-fluid">
<div class="panel-group" id="groups_accordion">
{$seen = 0}{$need_all = 0}
{foreach $groups_participants as $gp}
{$e = $gp['group']}
{$p = $gp['participants']}
{$tx = $gp['expenses']}
{$seen = $seen + 1}
{if !$show_all && $seen > $min_display && $e->tstamp < $now - $max_age*86400}{$need_all = 1}{break}{/if}
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#groups_accordion" href="#collapse_group_{$seen}">
{$e->name|escape}
        </a>
      </h4>
    </div>
    <div id="collapse_group_{$seen}" class="panel-collapse collapse {if $seen == 1}in{/if}">
      <div class="panel-body">
<div class="pull-right"><a href="{$prefix}display_group?group_id={$e->id}"><button class="btn btn-primary" type="submit">{t}Details{/t}</button></a></div>

{t}Participants:{/t}
{foreach $p as $a}
  {$a->nick}{if !$a@last}, {/if}
{/foreach}
<br />
{foreach $tx as $t}
{if $t->is_payback}{continue}{/if}
{t tn=$t->name}Last expense: %1{/t}
{break}
{/foreach}
    </div>
  </div>
  </div>

{/foreach}
    </div>
  </div>

{if $need_all}
<a href="{$prefix}?all=1"><button class="btn btn-primary center-block" type="submit">{t}Show older groups{/t}</button></a>
{/if}
{/if}

<div id="create-group-button" class="collapse in">
<button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#create-group-form" onclick="$('#create-group-button').removeClass('in');">{t}Create A Group{/t}</button>
</div>

<div class="collapse" id="create-group-form">
<h2>{t}Create A Group{/t}</h2>

<form method="{$form_method}" action="{$prefix}create_group" class="form-inline">
<div class="control-group">
<label class="control-label" for="name">{t}Name:{/t}</label>
<div class="controls">
<input name="name" type="text" placeholder="{t}Name{/t}"/>
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Create Group{/t}</button>
</div></div>
</form>
</div>

        </div>

        <div class="col-md-4">
<h2>{t}My Account{/t}</h2>

<table class="table"><tbody>
<tr><td>{t}Name{/t}</td><td>
{$logged_name|escape}
</td></tr>
<tr><td>{t}Email{/t}</td><td>
{$logged_user->email|escape}
</td></tr>
<tr><td>{t}Language{/t}</td><td>
{$logged_user->locale|@format_locale}
</td></tr>
</tbody></table>

<div>
<a href="{$prefix}my_account"><button class="btn btn-primary center-block" type="submit">{t}Edit{/t}</button></a>
</div>
        </div>
      </div>

{include file="../templates/footer.tpl"}
