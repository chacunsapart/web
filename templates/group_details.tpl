{include file="../templates/header.tpl" title="{$group->name|escape}"}

{$crumbs[] = [$group->name|escape, [ action => 'display_group', 'group_id' => $group->id ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{$group->name|escape}</h1>
<ul class="nav nav-tabs">
<li class="{if $active_tab == 'expenses'}active{/if}"><a href="#expenses" data-toggle="tab">{t}Expenses{/t}</a></li>
<li class="{if $active_tab == 'balances'}active{/if}"><a href="#balances" data-toggle="tab">{t}Balances{/t}</a></li>
<li class="pull-right {if $active_tab == 'options'}active{/if}"><a href="#options" data-toggle="tab">{t}Options{/t}</a></li>
</ul>

<div class="tab-content">
<div class="tab-pane fade {if $active_tab == 'expenses'}active in{/if}" id="expenses">
{$seen_expenses = 0}
{foreach $expenses as $expense}
  {if !$expense->is_payback} {$seen_expenses = 1} {break} {/if}
{/foreach}

{if $seen_expenses}
<div id="create-newexpense-button" class="collapse in">
<button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#create-newexpense-form" onclick="$('#create-newexpense-button').removeClass('in');">{t}Add An Expense{/t}</button>
</div>

<div class="collapse" id="create-newexpense-form">
{/if}
<h2>{t}Add An Expense{/t}</h2>

<form method="{$form_method}" action="{$prefix}create_expense" class="form-horizontal">
<input type="hidden" name="group_id" value="{$group->id}" />
<div class="control-group">
<label class="control-label" for="name">{t}Description:{/t}</label>
<div class="controls">
<input name="name" type="text" placeholder="{t}Description{/t}" />
</div></div>

<div class="control-group">
<label class="control-label" for="amount">{t}Amount:{/t}</label>
<div class="controls">
<input name="amount" type="number" min="{$group->cent}" step="{$group->cent}" placeholder="{t}Amount{/t}" />
</div></div>

<div class="control-group">
<label class="control-label" for="payer_id">{t}Payer:{/t}</label>
<div class="controls">
<select name="payer_id" onchange="if (this.value == 0) { $('#new_actor_input').addClass('in');} else { $('#new_actor_input').removeClass('in');}">
{foreach $potential_payers as $f}
  <option value="{$f->id}">{$f->nick|escape}</option>
{/foreach}
<option value="0">{t}New…{/t}</option>
</select>
</div></div>

<div class="control-group collapse" id="new_actor_input">
<label class="control-label" for="new_actor_nick" >{t}Name:{/t}</label>
<div class="controls">
<input name="new_actor_nick" type="text" placeholder="{t}New guest{/t}" />
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Add Expense{/t}</button>
</div></div>
</form>
{if $seen_expenses}
</div>

  <h2>{t en=$group->name}Expenses{/t}</h2>

<div class="container-fluid">
<div class="panel-group" id="expenses_accordion">
{$seen = 0}{$need_all = 0}
{$min_display = 10}{$max_age = 30}
{foreach $expenses as $expense}
  {if $expense->is_payback}{continue}{/if}
  {$seen = $seen + 1}
  {if !$show_all && $seen > $min_display && $expense->tstamp < $now - $max_age*86400}{$need_all = 1}{break}{/if}
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#expenses_accordion" href="#collapse_expense_{$seen}">
<div class="row">
  <div class="col-xs-6">{$expense->name|escape}</div>
  <div class="col-xs-3">{$expense->amount|@format_amount}</div>
  <div class="col-xs-3">
  {$expenseguests = 0}
  {$expenseparts = 0}
  {foreach $participations[$expense->id] as $participation}
    {$expenseguests = $expenseguests + 1}
    {$expenseparts = $expenseparts + $participation->parts}
  {/foreach}

  {if $expenseguests > 0}
    <span class="label label-success">{$expenseparts|format_part}</span>
  {else}
    <span class="label label-warning">{$expenseguests}</span>
  {/if}
</div>
</div>
        </a>
      </h4>
    </div>
    <div id="collapse_expense_{$seen}" class="panel-collapse collapse {if $seen == 1}in{/if}">
      <div class="panel-body">
  {if $expenseguests > 0}
  {capture assign=gl}
  {foreach $participations[$expense->id] as $participation}
    {$participation->guest->nick|escape}{if $participation->parts != $group->fraction}
 ({t escape=no plural="%1 parts" count=$participation->parts/ $group->fraction tp=$participation->parts|@format_part}%1 part{/t}){/if}{if !$participation@last}, {/if}{/foreach}{/capture}
  {t escape=no payer_nick=$expense->payer->nick|escape guestlist=$gl}Paid by %1; guests: %2.{/t}
  {else}
  {t escape=no payer_nick=$expense->payer->nick|escape}Paid by %1.{/t}
  <span class="label label-warning">{t}No guests{/t}</span>
  {/if}

<div style="display: inline" class="pull-right">
  <a href="{$prefix}display_expense?expense_id={$expense->id}"><button class="btn btn-primary" type="submit">{t}Edit{/t}</button></a>
  {if $expenseguests == 0}
    <form method="{$form_method}" action="{$prefix}remove_expense" class="inline">
    <input type="hidden" name="expense_id" value="{$expense->id}" />
    <button class="btn btn-warning" type="submit">{t}Remove{/t}</button>
    <input type="hidden" name="sure" value="1" />
    </form>
  {/if} {* expenseguests *}
</div>
      </div>
    </div>
  </div>
{/foreach}
</div>
</div> <!-- container-fluid -->
  {if $need_all}
    <a href="{$prefix}display_group?group_id={$group->id}&all_expenses=1"><button class="btn btn-primary center-block" type="submit">{t}Show older expenses{/t}</button></a>
  {/if} {* need_all *}
{/if} {* seen_expenses *}
</div>

<div class="tab-pane fade {if $active_tab == 'balances'}active in{/if}" id="balances">

{$seen_balances = 0}
{$need_invite = 0}    
{foreach $balances as $b}
  {$seen_balances = 1}
  {if ! $b.actor->account}
    {$need_invite = 1}
  {/if}
{/foreach}
{if ! $logged_user}
  {$need_invite = 0}    
{/if}

{if $seen_balances}
  <h2>{t}Group balances{/t}</h2>
  <table class="table">
  <thead><tr><th>{t}Guest{/t}</th><th>{t}Balance{/t}</th></tr></thead>
  <tbody>
{/if}

{$seen = 0}
{foreach $balances as $b}
  {$seen = $seen + 1}
  <tr>
  <td>
{$b.actor->nick|escape}
{if ! $b.actor->account}
      <form method="{$form_method}" action="{$prefix}invite" class="inline">
      <input type="hidden" name="actor_id" value="{$b.actor->id}" />
      <input type="hidden" name="from_group_id" value="{$group->id}" />
      <button class="btn btn-primary" type="submit">{t}Invite{/t}</button>
    </form>
{/if}
</td>
  <td><span class="label {if $b.balance > 0.01}label-success{elseif $b.balance < -0.01}label-warning{else}label-default{/if}">{$b.balance|@format_amount}</span></td></tr>
{/foreach} {* balances *}

  {if $seen_balances}
    </tbody></table>
  {/if}

{$seen = 0}
{foreach $suggestions as $s}
  {$seen = $seen + 1}
  {if $s@first && $s@total}
    {$maxamt = -1}
    
    <h2>{t}Suggested paybacks{/t}</h2>

<div class="container-fluid">
<div class="panel-group" id="suggestions_accordion">
  {/if}{* first *}

  {if $s.amount > $maxamt}
    {$maxamt = $s.amount}
    {$suggested_payer_id = $s.from->id}
    {$suggested_payee_id = $s.to->id}
  {/if} {* amount *}
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#suggestions_accordion" href="#collapse_suggestion_{$seen}">
<div class="row">
  <div class="col-xs-4">{$s.from->nick|escape}</div>
  <div class="col-xs-4">{$s.to->nick|escape}</div>
  <div class="col-xs-4">{$s.amount|@format_amount}</div>
</div>
        </a>
      </h4>
    </div>



    <div id="collapse_suggestion_{$seen}" class="panel-collapse collapse {if $seen == 1}in{/if}">
      <div class="panel-body">
  <form method="{$form_method}" action="{$prefix}create_payback" class="form-inline">
  <input type="hidden" name="group_id" value="{$group->id}" />
  <input type="hidden" name="amount" value="{$s.amount|@format_amount_form}" />
  <input type="hidden" name="payer_id" value="{$s.from->id}"/>
  <input type="hidden" name="payee_id" value="{$s.to->id}"/>
  <div class="input-append">
  <input name="comment" type="text" value="{$date}" />
  <button class="btn btn-primary" type="submit">{t}Done{/t}</button>
  </div>
  </form>
      </div>
    </div>
  </div>
  
  {if $s@last && $s@total}
  </div>
  
      <div id="create-payback-button" class="collapse in">
      <button class="btn btn-info center-block" type="button" data-toggle="collapse" data-target="#create-payback-form" onclick="$('#create-payback-button').removeClass('in');">{t}Manual payback{/t}</button>
      </div>
      
      <div class="collapse" id="create-payback-form">
      <h2>{t}Manual payback{/t}</h2>    
      <form method="{$form_method}" action="{$prefix}create_payback" class="form-horizontal">
      <input type="hidden" name="group_id" value="{$group->id}" />
      
      <div class="control-group">
      <label class="control-label" for="payer_id">{t}From:{/t}</label>
      <div class="controls">
      <select name="payer_id">
      <optgroup label="{t}Debtors{/t}">
      {foreach $balances as $b}
        {if $b.balance >= 0}{continue}{/if}
        <option value="{$b.actor->id}" {if $b.actor->id == $suggested_payer_id}selected="selected"{/if}>{$b.actor->nick|escape}</option>
      {/foreach}
      </optgroup>
      <optgroup label="{t}Creditors{/t}">
      {foreach $balances as $b}
        {if $b.balance < 0 }{continue}{/if}
        <option value="{$b.actor->id}">{$b.actor->nick|escape}</option>
      {/foreach}
      </optgroup>
      </select>
      </div></div>
      <div class="control-group">
      <label class="control-label" for="payee_id">{t}To:{/t}</label>
      <div class="controls">
      <select name="payee_id">
      <optgroup label="{t}Creditors{/t}">
      {foreach $balances as $b}
        {if $b.balance < 0}{continue}{/if}
        <option value="{$b.actor->id}" {if $b.actor->id == $suggested_payee_id}selected="selected"{/if}>{$b.actor->nick|escape}</option>
      {/foreach}
      </optgroup>
      <optgroup label="{t}Debtors{/t}">
      {foreach $balances as $b}
        {if $b.balance >= 0}{continue}{/if}
        <option value="{$b.actor->id}">{$b.actor->nick|escape}</option>
      {/foreach}
      </optgroup>
      </select>
      </div></div>
      <div class="control-group">
      <label class="control-label" for="amount">{t}Amount:{/t}</label>
      <div class="controls">
      <input name="amount" type="number" min="{$group->cent}" step="{$group->cent}" value="{$maxamt|@format_amount_form}" />
      </div></div>
      <div class="control-group">
      <label class="control-label" for="amount">{t}Comment:{/t}</label>
      <div class="controls">
      <input name="comment" type="text" value="{$date}" />
      </div></div>
      <div class="control-group">
      <div class="controls">
      <button class="btn btn-primary" type="submit">{t}Done{/t}</button>
      </div></div>
      </form>
      </div>
</div>
  {/if} {* last *}
{/foreach}

{$seen_paybacks = 0}
{foreach $expenses as $expense}
  {if $expense->is_payback} {$seen_paybacks = 1} {break} {/if}
{/foreach}

{if $seen_paybacks}
  <h2>{t en=$group->name}Paybacks Already Made{/t}</h2>
  

<div class="container-fluid">
<div class="panel-group" id="paybacks_accordion">
{/if} {* seen_paybacks *}

{$min_display = 10}{$max_age = 30}
{$seen = 0}{$need_all = 0}
{foreach $expenses as $expense}
  {if !$expense->is_payback} {continue} {/if}
  {$seen = $seen + 1}
  {if !$show_all && $seen > $min_display && $expense->tstamp < $now - $max_age*86400}{$need_all = 1}{break}{/if}

  {foreach $participations[$expense->id] as $participation}
    {$payee_nick = $participation->guest->nick} {break}
   {/foreach}

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#paybacks_accordion" href="#collapse_payback_{$seen}">
<div class="row">
  <div class="col-xs-4">  {$expense->payer->nick|escape}
</div>
  <div class="col-xs-4">{$payee_nick|escape}</div>
  <div class="col-xs-4">{$expense->amount|@format_amount}</div>
</div>
        </a>
      </h4>
    </div>

    <div id="collapse_payback_{$seen}" class="panel-collapse collapse">
      <div class="panel-body">

  {$expense->name}
    <form method="{$form_method}" action="{$prefix}remove_expense" class="inline pull-right">
    <input type="hidden" name="expense_id" value="{$expense->id}" />
    <button class="btn btn-warning" type="submit">{t}Remove{/t}</button>
    <input type="hidden" name="sure" value="1" />
    </form>
      </div>
    </div>
  </div>
{/foreach} {* expenses *}

{if $seen_paybacks}
  </div>
  {if $need_all}
    <a href="{$prefix}display_group?group_id={$group->id}&all_paybacks=1"><button class="btn btn-primary center-block" type="submit">{t}Show older paybacks{/t}</button></a>
  {/if} {* need_all *}
{/if} {* seen_paybacks *}

</div>
</div>

<div class="tab-pane fade {if $active_tab == 'options'}active in{/if}" id="options">

<h2>{t}Edit this group{/t}</h2>

<form method="{$form_method}" action="{$prefix}update_group" class="form-horizontal">
<input type="hidden" name="group_id" value="{$group->id}" />
<div class="control-group">
<label class="control-label" for="name">{t}Description:{/t}</label>
<div class="controls">
<input name="name" type="text" value="{$group->name|escape}" />
</div></div>

<div class="control-group">
<label class="control-label" for="name">{t}Minimal number of parts:{/t}</label>
<div class="controls">
<select name="fraction">
{for $i=1 to 4}
<option value="{$i}" {if $i == $group->fraction}selected="selected"{/if}>{$i|format_fraction}</option>
{/for}
</select>
</div></div>

<div class="control-group">
<label class="control-label" for="locale">{t}Locale:{/t}</label>
<div class="controls">
<select name="locale">
{foreach $locales_by_region as $region => $val}
  <optgroup label="{$val[0]['locale']|@format_locale_getregion}">
  {foreach $val as $l}
    <option value="{$l['locale']}" {if $l['locale'] == $group->locale}selected="selected"{/if}>{$l['locale']|@format_locale}</option>
  {/foreach}
  </optgroup>
{/foreach}
</select>
</div></div>

<div class="control-group">
<div class="controls">
<button class="btn btn-primary" type="submit">{t}Update{/t}</button>
</div></div>
</form>

<h2>{t}Invite a friend to this group{/t}</h2>

<div>
{t escape=no nick=$group->name|escape}To invite a friend to %1, send them the following link:{/t}
<a href="{$prefix}display_group?group_id={$group->id}&amp;group_cookie={$group_cookie}">{$group->name|escape}</a>
</div>
<div>
{t}You can also tell them to flash this QR-code:{/t}
</div>
{include '../templates/function__qr.tpl' p="display_group?group_id={$group->id}&group_cookie={$group_cookie}"}

  </div>

</div>

{include file="../templates/footer.tpl"}
