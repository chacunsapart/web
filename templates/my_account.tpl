{include file="../templates/header.tpl" title="{t}My Account{/t}"}

{capture assign=t}
{t}My Account{/t}
{/capture}
{$crumbs[] = [$t, [ action => 'my_account' ]]}
{include '../templates/function__crumbs.tpl'}

{include '../templates/function__feedback.tpl'}

<h1>{t}My Account{/t}</h1>

<ul class="nav nav-tabs">
<li class="{if $active_tab == 'general'}active{/if}"><a href="#general" data-toggle="tab">{t}General{/t}</a></li>
<li class="{if $active_tab == 'password'}active{/if}"><a href="#password" data-toggle="tab">{t}Password{/t}</a></li>
<li class="{if $active_tab == 'qr'}active{/if}"><a href="#qr" data-toggle="tab">{t}QR-code{/t}</a></li>
</ul>

<div class="tab-content">

  <div class="tab-pane fade {if $active_tab == 'general'}active in{/if}" id="general">
    <form method="{$form_method}" action="{$prefix}update_account" class="form-horizontal">
    <div class="control-group">
    <label class="control-label" for="name">{t}Name:{/t}</label>
    <div class="controls">
    <input name="nick" type="text" value="{$logged_name|escape}" />
    </div></div>

    <div class="control-group">
    <label class="control-label" for="email">{t}Email:{/t}</label>
    <div class="controls">
    <input name="email" type="email" value="{$logged_user->email|escape}" />
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="locale">{t}Preferred language:{/t}</label>
    <div class="controls">
    <select name="locale">
    {foreach $locales_by_lang as $lang => $val}
    {if $val|@count > 1}
    <optgroup label="{$val[0]['locale']|@format_locale_getlang}">
    {/if}
    {foreach $val as $l}
    <option value="{$l['locale']}" {if $l['locale'] == $logged_user->locale}selected="selected"{/if}>{$l['locale']|@format_locale}</option>
    {/foreach}
    {if $val|@count > 1}
    </optgroup>
    {/if}
    {/foreach}
    </select>
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="password">{t}Password:{/t}</label>
    <div class="controls">
    <input name="password" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    <div class="control-group">
    <div class="controls">
    <button class="btn btn-primary" type="submit">{t}Update{/t}</button>
    </div></div>
    </form>
  </div>

  <div class="tab-pane fade {if $active_tab == 'password'}active in{/if}" id="password">
    <form method="{$form_method}" action="{$prefix}change_password" class="form-horizontal">
    <div class="control-group">
    <label class="control-label" for="oldpassword">{t}Old Password:{/t}</label>
    <div class="controls">
    <input name="oldpassword" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="newpassword1">{t}New Password:{/t}</label>
    <div class="controls">
    <input name="newpassword1" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    <div class="control-group">
    <label class="control-label" for="newpassword2">{t}Confirm New Password:{/t}</label>
    <div class="controls">
    <input name="newpassword2" type="password" placeholder="{t}Password{/t}" />
    </div></div>
    
    <div class="control-group">
    <div class="controls">
    <button class="btn btn-primary" type="submit">{t}Update{/t}</button>
    </div></div>
    </form>
  </div>

  <div class="tab-pane fade {if $active_tab == 'qr'}active in{/if}" id="qr">
    {$link="{$prefix}?auth_cookie={$logged_user->auth_cookie}"}
    <div>
    {t}To log in automatically, you could bookmark the following link:{/t}
    <a href="{$prefix}?auth_cookie={$logged_user->auth_cookie}">{$sn_styled}</a>.
    </div>
    <div>
    {t}You can also flash this QR-code:{/t}
    </div>
    {include '../templates/function__qr.tpl' p="?auth_cookie={$logged_user->auth_cookie}"}
  </div>
</div>


{include file="../templates/footer.tpl"}
