#! /bin/sh

sizes="l:36 m:48 h:72 xh:96 xxh:144"

cd images
for s in $sizes ; do
    n=drawable_${s%:*}dpi
    w=${s#*:}
    
    mkdir -p icons/$n
    rsvg -w $w -h $w logo.svg icons/$n/ic_launcher.png
done

n=drawable_nodpi
mkdir -p icons/$n
rsvg logo.svg icons/$n/logo.png

tar caf ../root/icons.tar icons
rm -r icons

huge=3000
final=300
small=30

rsvg -w ${huge} -h ${huge} logo.svg ../root/logo-huge.png
cd ../root/
convert logo-huge.png -scale ${final}x${final} logo.png
convert logo.png -channel RGBA -fill '#ffffff00' -opaque '#00000000' logo.png
convert logo-huge.png -scale ${small}x${small} logo-small.png
composite -blend 25% logo.png -size ${final}x${final} xc:none logo-clear.png
rm logo-huge.png
