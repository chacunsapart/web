#! /bin/sh

prefix=${prefix:-/srv/chacunsapart}

su - postgres -c "pg_dump -s chacunsapart" > $prefix/doc/schema.sql
su - postgres -c "postgresql_autodoc -d chacunsapart -t dot"
dot -Tpng ~postgres/chacunsapart.dot  > $prefix/doc/chacunsapart.png
rm ~postgres/chacunsapart.dot
