#! /bin/sh

prefix=${prefix:-/srv/chacunsapart}

pgbadger --nograph --disable-hourly --disable-session --disable-connection --exclude-query "^(BEGIN|COMMIT|ROLLBACK|RESET)" --dbuser chacunsapart -o $prefix/root/pgbadger.html.new /var/log/postgresql/*
mv $prefix/root/pgbadger.html.new $prefix/root/pgbadger.html
