<?php

namespace Cygol3;

Cygol3::$logged_user = NULL;
Cygol3::$current_actor = NULL;

function set_cookie($name, $value, $duration = 86400) {
	setcookie($name, $value, time()+$duration, '/', Cygol3::$config['hostname'], true, true);
}

function setup_session($user) {
	if ($user) {
		set_cookie('auth_cookie', $user->auth_cookie, 864000);
		Cygol3::$current_actor =& $user->get_actor();
		set_language($user->locale);
		set_cookie('locale', Cygol3::$lang, 8640000);
		process_known_locales();
	} else {
		set_cookie('auth_cookie', '', 0);
		Cygol3::$current_actor = NULL;
		$lang = '';
		if (getStringFromRequest('set_default_locale')) {
			set_language(getStringFromRequest('set_default_locale'));
			set_cookie('locale', Cygol3::$lang, 8640000);
		} elseif (getStringFromCookie('locale')) {
			set_language(getStringFromCookie('locale'));
		} elseif (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			set_language(locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']));
		}
	}
	Cygol3::$logged_user =& $user;
	export_view_param('logged_user', Cygol3::$logged_user);
	export_view_param('current_actor', Cygol3::$current_actor);
}

function do_auth() {
	Cygol3::$cookies = _getPredefinedArray('_COOKIE', 'HTTP_COOKIE_VARS');
	$potential_cookies = array(getStringFromRequest('auth_cookie'),
				   getStringFromCookie('auth_cookie'));

	$u = NULL;
	foreach ($potential_cookies as $auth_cookie) {
		if ($auth_cookie != '') {
			try {
				$u = get_account_by_auth_cookie($auth_cookie);
				break;
			}
			catch (NotFoundException $e) {
				$u = NULL;
			}
		}
	}
	setup_session($u);
}
