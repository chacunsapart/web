<?php

namespace Cygol3;

class DataException extends \Exception { }
class DuplicateException extends DataException { }
class NotFoundException extends DataException { }
class NotAllowedException extends DataException { }
class BadAmountException extends DataException { }
class EmptyValueException extends DataException { }
class InvalidValueException extends DataException { }
class TooLongException extends DataException { }

class DBException extends \Exception { }
