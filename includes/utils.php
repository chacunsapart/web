<?php

namespace Cygol3;

class TypeException extends \Exception { }

function _getStringFromArray( $array, $key, $defaultValue) {
	if (isset($array[$key])) {
		return $array[$key];
	}
	return $defaultValue;
}
function _unlocalize_float($string){
    return floatval($floatString);
}
function _getFloatFromArray( $array, $key, $defaultValue) {
	if (isset($array[$key])) {
		$LocaleInfo = localeconv();
		$unloc = $array[$key];
		$unloc = str_replace($LocaleInfo["mon_decimal_point"] , ".", $unloc);
		if (is_numeric($unloc)) {
			return floatval($unloc);
		}
	}
	if (is_float($defaultValue)) {
		return $defaultValue;
	}
	if (is_int($defaultValue) || is_numeric($defaultValue)) {
		return floatval($defaultValue);
	}
	throw new TypeException(_("Wrong value for float."));
}
function _getIntFromArray( $array, $key, $defaultValue) {
	if (isset($array[$key]) && is_numeric($array[$key])) {
		return intval($array[$key]);
	}
	if (is_numeric($defaultValue)) {
		return intval($defaultValue);
	}
	throw new TypeException(_("Wrong value for int."));
}

function _getPredefinedArray($superGlobalName, $oldName) {
	if(isset($GLOBALS[$superGlobalName])) {
		$array = & $GLOBALS[$superGlobalName];
	} elseif(isset($GLOBALS[$oldName])) {
		$array = & $GLOBALS[$oldName];
	} else {
		$array = array();
	}
	return $array;
}
function _getPostArray() {
	return _getPredefinedArray('_POST', 'HTTP_POST_VARS');
}
function _getGetArray() {
	return _getPredefinedArray('_GET', 'HTTP_GET_VARS');
}
function _getCookieArray() {
	return _getPredefinedArray('_COOKIE', 'HTTP_COOKIE_VARS');
}
function _getRequestArray() {
	if(isset($_REQUEST)) {
		return $_REQUEST;
	} else {
		return array_merge($GLOBALS['HTTP_GET_VARS'], $GLOBALS['HTTP_POST_VARS'], $GLOBALS['HTTP_COOKIE_VARS']);
	}
}

function getStringFromPost($key, $defaultValue = '') {
	return _getStringFromArray(_getPostArray(), $key, $defaultValue);
}
function getStringFromGet($key, $defaultValue = '') {
	return _getStringFromArray(_getGetArray(), $key, $defaultValue);
}
function getStringFromRequest($key, $defaultValue = '') {
	return _getStringFromArray(_getRequestArray(), $key, $defaultValue);
}
function getStringFromCookie($key, $defaultValue = '') {
	return _getStringFromArray(Cygol3::$cookies, $key, $defaultValue);
}
function getFloatFromPost($key, $defaultValue = 0.0) {
	return _getFloatFromArray(_getPostArray(), $key, $defaultValue);
}
function getFloatFromGet($key, $defaultValue = 0.0) {
	return _getFloatFromArray(_getGetArray(), $key, $defaultValue);
}
function getFloatFromRequest($key, $defaultValue = 0.0) {
	return _getFloatFromArray(_getRequestArray(), $key, $defaultValue);
}
function getFloatFromCookie($key, $defaultValue = 0.0) {
	return _getFloatFromArray(Cygol3::$cookies, $key, $defaultValue);
}
function getIntFromPost($key, $defaultValue = 0) {
	return _getIntFromArray(_getPostArray(), $key, $defaultValue);
}
function getIntFromRequest($key, $defaultValue = 0) {
	return _getIntFromArray(_getRequestArray(), $key, $defaultValue);
}
function getIntFromGet($key, $defaultValue = 0) {
	return _getIntFromArray(_getGetArray(), $key, $defaultValue);
}
function getIntFromCookie($key, $defaultValue = 0) {
	return _getIntFromArray(Cygol3::$cookies, $key, $defaultValue);
}

function util_randbytes($num=8) {
	$b = '';

	$f = @fopen("/dev/urandom", "rb");
	if ($f !== FALSE) {
		$b .= @fread($f, $num);
		fclose($f);
	}
	// Hm.  No /dev/urandom?  Try /dev/random.
	if (strlen($b) < $num) {
		$f = @fopen("/dev/random", "rb");
		if ($f !== FALSE) {
			$b .= @fread($f, $num);
			fclose($f);
		}
	}
	// Still no luck?  Fall back to PHP's built-in PRNG
	while (strlen($b) < $num) {
		$b .= uniqid(mt_rand(), true);
	}

	$b = substr($b, 0, $num);
	return ($b);
}

function amount_to_db($amount) {
	$LocaleInfo = localeconv();
	return (int) round($amount * pow(10, $LocaleInfo['frac_digits']));
}

function amount_from_db($amount) {
	$LocaleInfo = localeconv();
	$amount = (int) $amount;
	return $amount / pow(10, $LocaleInfo['frac_digits']);
}

function format_amount($amount) {
	return preg_replace('/ /', ' ', money_format("%n", $amount));
}
function format_amount_form($amount) {
	$LocaleInfo = localeconv();
	$digits = $LocaleInfo['frac_digits'];
	return sprintf("%.".$digits."F",$amount);
	return money_format("%!n", $amount);
}


function encode_password($password) {
	validate_password($password);

	// return \password_hash($password, PASSWORD_DEFAULT);

	$seed = substr(md5(util_randbytes()),0,22);
	return crypt($password,'$2a$07$'.$seed.'$');
}

function check_password($password, $stored_value) {
	// return \password_verify($password, $stored_value);

	if (crypt($password, $stored_value) == $stored_value) {
		return true;
	} else {
		return false;
	}
}

function perform_action($action) {
	global $smarty;

	require("actions/$action.php");

	exit (0);
}

function show_view($view) {
	global $smarty;

	foreach (Cygol3::$view_params as $name => $value) {
		$$name = $value;
	}

	if (Cygol3::$logged_user) {
		$smarty->assign('logged_user', Cygol3::$logged_user);
		$smarty->assign('logged_name', Cygol3::$current_actor->nick);
	} else {
		$smarty->assign('logged_user', NULL);
		$smarty->assign('logged_name', '');
	}

	$sn = _('Chacun sa part');
	$sn = _(Cygol3::$config['sn']);
	$stylesheet = _('style-en.css');

	$smarty->assign('sn', $sn);
	$smarty->assign('stylesheet', $stylesheet);
	$smarty->assign('sn_styled', '<span class="csp">'.$sn.'</span>');
	$smarty->assign('prefix', Cygol3::$config['prefix']);
	$smarty->assign('form_method', Cygol3::$config['form_method']);
	$smarty->assign('form_method_ro', Cygol3::$config['form_method_ro']);
	$smarty->assign('locales_by_lang', Cygol3::$locales_by_lang);
	$smarty->assign('locales_by_region', Cygol3::$locales_by_region);

	$main_locales = array();
	foreach (Cygol3::$config['main_locales'] as $l) {
		$main_locales[] = array("locale" => $l,
					"display_name" => locale_get_display_language($l,$l));
	}
	$smarty->assign('main_locales', $main_locales);

	$smarty->assign('crumbs', array(array(_('Home'), array('action' => ''))));

	require("views/$view.php");

	exit (0);
}

function export_view_param($name, $value) {
	Cygol3::$view_params[$name] = $value;
}

function my_encode_b64($f) {
	$f = base64_encode($f);
	$f = str_replace('+', '-', $f);
	$f = str_replace('/', '_', $f);
	return $f;
}

function my_decode_b64($f) {
	$f = str_replace('-', '+', $f);
	$f = str_replace('_', '/', $f);
	$f = base64_decode($f,true);
	return $f;
}

function number_magnitude($n) {
	$mult = 1;
	while ($n > $mult * 10) {
		$mult *= 10;
	}
	return $mult * (int) round ($n / $mult);
}

function sprintf_number($n) {
	$LocaleInfo = localeconv();
	$grouping = $LocaleInfo['grouping'];

	$s = sprintf("%d", $n);
	$done = '';

	if ($grouping) {
		$i = 0;
		$g = $grouping[$i];

		while (strlen($s) > $g) {
			if ($g == CHAR_MAX) {
				break;
			}
			$j = 0;
			while ($grouping[$i-$j] == 0) {
				$j++;
			}
			$g = $grouping[$i-$j];
			$done = $LocaleInfo['thousands_sep'].substr($s, -$g).$done;
			$s = substr($s, 0, -$g);
			if ($i < count($grouping) - 1) {
				$i++;
				$g = $grouping[$i];
			}
		}

	}
	return $s.$done;

}

function format_fraction($n) {
	switch ($n) {
	case 1:
		return 1;
	case 2:
		return "½";
	case 3:
		return "⅓";
	case 4:
		return "¼";
	}
}

function format_part($n) {
	if (Cygol3::$current_group) {
		$f = Cygol3::$current_group->fraction;
	} else {
		$f = 1;
	}

	if ($f == 1) {
		return $n;
	}

	$r = $n % $f;
	$d = ($n - $r) / $f;
	if ($d == 0) {
		$p = '';
	} else {
		$p = "$d ";
	}
	switch ($f) {
	case 2:
		switch ($r) {
		case 0:
			return $d;
		case 1:
			return $p."½";
		}
	case 3:
		switch ($r) {
		case 0:
			return $d;
		case 1:
			return $p."⅓";
		case 2:
			return $p."⅔";
		}
	case 4:
		switch ($r) {
		case 0:
			return $d;
		case 1:
			return $p."¼";
		case 2:
			return $p."½";
		case 3:
			return $p."¾";
		}
	}
}

function setup_current_group($g) {
	Cygol3::$current_group =& $g;

	setlocale (LC_MONETARY, $g->locale);
	setlocale (LC_NUMERIC, $g->locale);
	$g->cent = format_amount_form(amount_from_db(1));
}

function check_and_redirect($group) {
	if ($group->check_access()) {
		return true;
	}

 	if (Cygol3::$logged_user) {
		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}
}

function get_potential_payers($group) {
	$seen = array();

	$other_group_participants = array();
	foreach ($group->get_participants() as $eg) {
		if (!isset($seen[$eg->id])) {
			$other_group_participants[] = $eg;
			$seen[$eg->id] = true;
		}
	}

	$other_friends = array();
	if (Cygol3::$logged_user) {
		if (!isset($seen[Cygol3::$current_actor->id])) {
			$other_friends[] = Cygol3::$current_actor;
			$seen[Cygol3::$current_actor->id] = true;
		}
	}

	foreach (get_all_friends() as $f) {
		if (!isset($seen[$f->id])) {
			$other_friends[] = $f;
			$seen[$f->id] = true;
		}
	}

	$potential_payers = array_merge($other_group_participants,
					$other_friends);

	return $potential_payers;
}

function cmp_actors_by_nick($a, $b) {
	return strcoll($a->nick, $b->nick);
}

function cmp_participants_by_nick($a, $b) {
	return strcoll($a['guest']->nick, $b['guest']->nick);
}

function cmp_participations_by_nick($a, $b) {
	return strcoll($a->guest->nick, $b->guest->nick);
}

function gather_stats() {
	$stats = array();

	$r = db_query("SELECT count(*) from accounts");
	$row = pg_fetch_row($r);
	$stats['accounts'] = $row[0];

	$r = db_query("SELECT count(*) from actors");
	$row = pg_fetch_row($r);
	$stats['actors'] = $row[0];

	$r = db_query("SELECT count(*) from groups");
	$row = pg_fetch_row($r);
	$stats['groups'] = $row[0];

	$r = db_query("SELECT count(*) from groups WHERE tstamp > $1",
		      array(time() - 7 * 86400));
	$row = pg_fetch_row($r);
	$stats['groups_recent'] = $row[0];

	$r = db_query("SELECT count(*) from expenses");
	$row = pg_fetch_row($r);
	$stats['expenses'] = $row[0];

	$r = db_query("SELECT count(*) from expenses WHERE tstamp > $1",
		      array(time() - 7 * 86400));
	$row = pg_fetch_row($r);
	$stats['expenses_recent'] = $row[0];

	foreach ($stats as $k => $v) {
		$stats[$k] = sprintf_number(number_magnitude($v));
	}

	return $stats;
}

function expire_expirable_maybe($p) {
	$r = rand()/getrandmax();
	if ($r < $p) {
		error_log("Expiring old stuff because $r < $p.");
		expire_expirable();
	}
}

function expire_expirable() {
	expire_old_recovery_codes();
	expire_old_invitations();
}

function cmp_int($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

function cmp_groups_by_reverse_tstamp($a, $b) {
	if ($a->tstamp < $b->tstamp) {
		return 1;
	} elseif ($a->tstamp > $b->tstamp) {
		return -1;
	} else {
		if ($a->id < $b->id) {
			return 1;
		} elseif ($a->id > $b->id) {
			return -1;
		} else {
			return 0;
		}
	}
}

function merge_group_list($l1, $l2) {
	$result = array();
	$seen = array();

	while (count($l1) && count($l2)) {
		if (cmp_groups_by_reverse_tstamp($l1[0], $l2[0]) < 0) {
			$t = array_shift($l1);
		} else {
			$t = array_shift($l2);
		}
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	while (count($l1)) {
		$t = array_shift($l1);
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	while (count($l2)) {
		$t = array_shift($l2);
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	return $result;
}

function merge_user_list($l1, $l2) {
	$result = array();
	$seen = array();

	while (count($l1) && count($l2)) {
		if (cmp_actors_by_nick($l1[0], $l2[0]) < 0) {
			$t = array_shift($l1);
		} else {
			$t = array_shift($l2);
		}
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	while (count($l1)) {
		$t = array_shift($l1);
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	while (count($l2)) {
		$t = array_shift($l2);
		if (!isset($seen[$t->id])) {
			$result[] = $t;
			$seen[$t->id] = 1;
		}
	}
	return $result;
}
