<?php

namespace Cygol3;

function db_connect($params) {
	$cstring = "host=".$params['db_host']." dbname=".$params['db_name']." user=".$params['db_user'] ." password=".$params['db_pass'];

	Cygol3::$db = pg_pconnect($cstring);

	if (!Cygol3::$db) {
		throw new DBException('Could not connect to database');
	}
}

function db_query($query, $params = NULL) {
	if ($params === NULL) {
		$r = pg_query(Cygol3::$db, $query);
	} else {
		$r = pg_query_params(Cygol3::$db, $query, $params);
	}
	if (!$r) {
		throw new DBException(pg_last_error());
	}
	return $r;
}

function db_begin() {
	// start database transaction only for the top-level
	// programmatical transaction
	Cygol3::$_db_transaction_level++;
	if (Cygol3::$_db_transaction_level == 1) {
		return db_query("BEGIN");
	}

	return true;
}

function db_commit($dbserver = NULL) {
	if (Cygol3::$_db_transaction_level == 0) {
		throw new DBException("COMMIT underflow");
	}

	Cygol3::$_db_transaction_level--;
	if (Cygol3::$_db_transaction_level == 0) {
		return db_query("COMMIT");
	}
}

function db_rollback($dbserver = NULL) {
	if (Cygol3::$_db_transaction_level == 0) {
		throw new DBException("ROLLBACK underflow");
	}

	Cygol3::$_db_transaction_level--;
	if (Cygol3::$_db_transaction_level == 0) {
		return db_query("ROLLBACK");
	}
}
