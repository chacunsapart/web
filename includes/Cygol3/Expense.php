<?php

namespace Cygol3 ;

class Expense {
	var $id = 0;
	var $name = '';
	var $amount = 0;
	var $group = NULL;
	var $payer = NULL;
	var $is_payback = 0;
	var $tstamp = 0;

	function __construct($id) {
		$this->load($id);
	}

	function load($id) {
		$r = db_query('SELECT * FROM expenses WHERE id=$1',
			      array($id));

		$result = array();
		if ($row = pg_fetch_assoc($r)) {
			$this->id = $row['id'];
			$this->name = $row['name'];
			$this->amount = amount_from_db($row['amount']);
			$this->group =& ObjectCache::retrieve_object('Group', $row['group_id']);
			$this->payer =& ObjectCache::retrieve_object('Actor', $row['payer_actor']);
			$this->is_payback = $row['is_payback'];
			$this->tstamp = $row['tstamp'];
		} else {
			throw new NotFoundException(_("No expense with this id."));
		}
	}

	function save() {
		validate_expense_name($this->name);

		if ($this->amount <= 0) {
			throw new BadAmountException(_("Amounts must be positive."));
		}
		if ($this->group == NULL) {
			throw new EmptyValueException(_("No group."));
		}
		if ($this->payer == NULL) {
			throw new EmptyValueException(_("No payer."));
		}

		db_begin();

		$this->tstamp = time();
		$r = db_query('UPDATE expenses SET name=$1, amount=$2, group_id=$3, payer_actor=$4, is_payback=$5, tstamp=$6 WHERE id=$7',
			      array($this->name, amount_to_db($this->amount), $this->group->id, $this->payer->id, $this->is_payback, $this->tstamp, $this->id));
		$this->group->clear_cache();
		$this->group->save();

		db_commit();
	}

	function delete($full = false) {
		db_begin();

		if ($full) {
			$r = db_query('DELETE FROM participations WHERE expense_id=$1',
				      array($this->id));
		}

		$r = db_query('SELECT id FROM participations WHERE expense_id=$1',
			      array($this->id));
		if (pg_fetch_row($r)) {
			db_rollback();
			throw new NotAllowedException(_("Can't delete a non-empty expense."));
		}

		$r = db_query('DELETE FROM expenses WHERE id=$1',
			      array($this->id));
		$this->group->clear_cache();
		$this->group->save();

		$id = 0;
		$name = '';
		$amount = 0;
		$group = NULL;
		$payer = NULL;
		$is_payback = 0;
		$tstamp = 0;

		db_commit();
	}

	function get_participations() {
		$r = db_query('SELECT id FROM participations WHERE expense_id=$1 ORDER BY tstamp DESC, id DESC',
			      array($this->id));
		$result = array();
		while ($row = pg_fetch_assoc($r)) {
			$result[] =& ObjectCache::retrieve_object('Participation', $row['id']);
		}

		return $result;
	}
}
