<?php

namespace Cygol3 ;

class Actor {
	var $id = 0;
	var $nick = 0;
	var $account = NULL;

	function __construct($id, $data=array()) {
		$this->load($id, $data);
	}

	function load($id, $data=array()) {
		if (count($data)) {
			$row = $data;
		} else {
			$r = db_query('SELECT * FROM actors WHERE id=$1',
				      array($id));

			if (!$row = pg_fetch_assoc($r)) {
				throw new NotFoundException(_("No actor with this id."));
			}
		}

		$this->id = $row['id'];
		$this->nick = $row['nick'];

	}

	function fetch_account() {
		$r = db_query('SELECT * FROM accounts WHERE actor_id=$1',
			      array($this->id));
		if ($row = pg_fetch_assoc($r)) {
			try {
				$this->account =& ObjectCache::retrieve_object('Account', $row['id']);
			}
			catch (NotFoundException $e) {
				$this->account = NULL;
			}
		} else {
			$this->account = NULL;
		}
	}

	function save() {
		validate_nick($this->nick);

		$r = db_query('UPDATE actors SET nick=$1 WHERE id=$2',
			      array($this->nick, $this->id));
	}

	function delete() {
		$r = db_query('DELETE FROM actors WHERE id=$1',
			      array($this->id));
		$this->id = 0;
		$this->nick = 0;
		$this->account = NULL;
	}
}
