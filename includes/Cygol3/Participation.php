<?php

namespace Cygol3 ;

class Participation {
	var $id = 0;
	var $expense = NULL;
	var $guest = NULL;
	var $parts = 0;
	var $tstamp = 0;

	function __construct($id) {
		$this->load($id);
	}

	function load($id) {
		$r = db_query('SELECT * FROM participations WHERE id=$1',
			      array($id));

		$result = array();
		if ($row = pg_fetch_assoc($r)) {
			$this->id = $row['id'];
			$this->expense =& ObjectCache::retrieve_object('Expense', $row['expense_id']);
			$this->guest =& ObjectCache::retrieve_object('Actor', $row['guest_actor']);
			$this->parts = $row['parts'];
			$this->tstamp = $row['tstamp'];
		} else {
			throw new NotFoundException(_("No guest participation with this id."));
		}
	}

	function save() {
		db_begin();

		$this->tstamp = time();
		$r = db_query('UPDATE participations SET expense_id=$1, guest_actor=$2, parts=$3, tstamp=$4 WHERE id=$5',
			      array($this->expense->id, $this->guest->id, $this->parts, $this->tstamp, $this->id));
		$this->expense->group->clear_cache();
		$this->expense->save();

		db_commit();
	}

	function delete() {
		db_begin();

		$r = db_query('DELETE FROM participations WHERE id=$1',
			      array($this->id));
		$this->expense->group->clear_cache();
		$this->expense->save();

		$this->id = 0;
		$this->expense = NULL;
		$this->guest = NULL;
		$this->parts = 0;
		$this->tstamp = 0;

		db_commit();
	}
}
