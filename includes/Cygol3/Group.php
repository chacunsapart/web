<?php

namespace Cygol3 ;

class Group {
	var $id = 0;
	var $name = '';
	var $creator = NULL;
	var $tstamp = 0;
	var $status = 1;
	var $invitation_cookie = '';
	var $locale = '';
	var $fraction = 0;
	var $cent = 0;

	private $_cached_balance_data = NULL;
	private $_cached_suggestion_data = NULL;

	static $_current = NULL;

	function __construct($id) {
		$this->load($id);
	}

	function load($id) {
		$r = db_query('SELECT * FROM groups WHERE id=$1',
			      array($id));

		$result = array();
		if ($row = pg_fetch_assoc($r)) {
			$this->id = $row['id'];
			$this->name = $row['name'];
			$this->creator =& ObjectCache::retrieve_object('Actor', $row['creator_actor']);
			$this->tstamp = $row['tstamp'];
			$this->status = $row['status'];
			$this->invitation_cookie = $row['invitation_cookie'];
			$this->locale = $row['locale'];
			$this->fraction = $row['fraction'];
		} else {
			throw new NotFoundException(_("No group with this id."));
		}
		$this->clear_cache();
	}

	function clear_cache() {
		$this->_cached_balance_data = NULL;
		$this->_cached_suggestion_data = NULL;
	}

	function save() {
		validate_group_name($this->name);
		validate_locale($this->locale);
		validate_group_cookie($this->invitation_cookie);
		validate_group_fraction($this->fraction);

		db_begin();

		$r = db_query('SELECT fraction FROM groups WHERE id=$1',
			      array($this->id));
		$row = pg_fetch_assoc($r);
		$oldfrac = $row['fraction'];

		if ($this->fraction != $oldfrac) {
			if ($this->fraction % $oldfrac == 0) {
				$r = db_query('UPDATE participations SET parts=parts * $1 WHERE expense_id in (SELECT id FROM expenses WHERE group_id=$2)',
					      array($this->fraction / $oldfrac,
						    $this->id));
			} elseif ($oldfrac % $this->fraction == 0) {
				$div = $oldfrac / $this->fraction;

				foreach ($expenses = $this->get_expenses() as $e) {
					$oksofar = true;
					foreach ($e->get_participations() as $p) {
						if ($p->parts % $div != 0) {
							$oksofar = false;
							break;
						}
					}
					if ($oksofar) {
						$r = db_query('UPDATE participations SET parts=parts / $1 WHERE expense_id=$2',
							      array($div,
								    $e->id));
						foreach ($e->get_participations() as $p) {
							$p->load($p->id);
						}
					}
				}
			} else {
				foreach ($expenses = $this->get_expenses() as $e) {
					$oksofar = true;
					foreach ($e->get_participations() as $p) {
						if ($p->parts % $oldfrac != 0) {
							$oksofar = false;
							break;
						}
					}
					if ($oksofar) {
						$r = db_query('UPDATE participations SET parts=(parts * $1) / $2 WHERE expense_id=$3',
							      array($this->fraction,
								    $oldfrac,
								    $e->id));
						foreach ($e->get_participations() as $p) {
							$p->load($p->id);
						}
					} else {
						$r = db_query('UPDATE participations SET parts=(parts * $1) WHERE expense_id=$2',
							      array($this->fraction,
								    $e->id));
						foreach ($e->get_participations() as $p) {
							$p->load($p->id);
						}
					}
				}
			}
		}

		$this->tstamp = time();
		$r = db_query('UPDATE groups SET name=$1, creator_actor=$2, tstamp=$3, status=$4, invitation_cookie=$5,locale=$6,fraction=$7 WHERE id=$8',
			      array($this->name, $this->creator->id, $this->tstamp, $this->status, $this->invitation_cookie, $this->locale, $this->fraction, $this->id));
		$this->clear_cache();

		db_commit();
	}

	function check_access() {
		if (Cygol3::$logged_user) {
			$r = db_query('SELECT group_id FROM group_actor_vw WHERE acct_id=$1 AND group_id=$2 LIMIT 1',
				      array(Cygol3::$logged_user->id, $this->id));

			if ($row = pg_fetch_assoc($r)) {
				return true;
			}
		}

		$group_cookies = get_group_cookies();
		foreach ($group_cookies as $cookie) {
			$r = db_query('SELECT id FROM groups WHERE id=$1 AND invitation_cookie=$2 LIMIT 1',
				      array($this->id, $cookie));
			if ($row = pg_fetch_assoc($r)) {
				return true;
			}
		}

		return 0;
	}

	function get_expenses() {
		$r = db_query('SELECT id FROM expenses WHERE group_id=$1 ORDER BY tstamp DESC, id DESC',
			      array($this->id));
		$result = array();
		while ($row = pg_fetch_assoc($r)) {
			$result[] =& ObjectCache::retrieve_object('Expense', $row['id']);
		}

		return $result;
	}

	function get_participants() {
		$result = array();

		$seen = array();

		$seen[$this->creator->id] = 1;
		$result[] = $this->creator;

		foreach ($this->get_expenses() as $expense) {
			if (! isset($seen[$expense->payer->id])) {
				$seen[$expense->payer->id] = 1;
				$result[] = $expense->payer;
			}

			foreach ($expense->get_participations() as $participation) {
				if (isset($seen[$participation->guest->id])) {
					continue;
				}
				$seen[$participation->guest->id] = 1;
				$result[] = $participation->guest;
			}
		}

		return $result;
	}

	private function _get_balances_data() {
		if ($this->_cached_balance_data != NULL) {
			return $this->_cached_balance_data;
		}

		$dbp = array($this->id);

		$r = db_query('SELECT * FROM group_participations_vw WHERE group_id=$1 ORDER BY expense_id, participation_id', $dbp);

		$data = array();
		$balances = array();
		$seen = array();
		$scm = 1;
		while ($row = pg_fetch_assoc($r)) {
			$expense_id = $row['expense_id'];
			$data[$expense_id]['payer_actor'] = $row['payer_actor'];
			$data[$expense_id]['amount'] = $row['amount'];
			$participation_id = $row['participation_id'];
			$data[$expense_id]['parts'][$participation_id] = array('guest_actor' => $row['guest_actor'],
							       'parts' => $row['parts']);
			$balances[$row['payer_actor']] = 0;
			$balances[$row['guest_actor']] = 0;
			$seen[$row['payer_actor']] = 1;
		}

		foreach ($data as $expense_id => $expensed) {
			$data[$expense_id]['total_parts'] = 0;
			foreach ($expensed['parts'] as $participation_id => $participationd) {
				$data[$expense_id]['total_parts'] += $participationd['parts'];
				$seen[$participationd['guest_actor']] = 1;
			}
			$gcd = gmp_gcd($scm, $data[$expense_id]['total_parts']);
			$scm = gmp_div(gmp_mul($scm, $data[$expense_id]['total_parts']), $gcd);
		}
		$scm = intval(gmp_strval($scm));

		$total_am = 0;
		foreach ($data as $expense_id => $expensed) {
			$expenseam = $expensed['amount'] * $scm;
			$total_am += $expenseam;
			$balances[$expensed['payer_actor']] += $expenseam;
			$left = $expenseam;
			foreach ($expensed['parts'] as $participation_id => $participationd) {
				$p = ($expenseam * $participationd['parts']) / $expensed['total_parts'];
				if (!is_integer ($p)) {
					$p = round($p);
				}
				$balances[$participationd['guest_actor']] -= $p;
				$left -= $p;
			}
			while ($left != 0) {
				foreach ($expensed['parts'] as $participation_id => $participationd) {
					if ($left > 0) {
						$balances[$participationd['guest_actor']]--;
						$left--;
					} elseif ($left < 0) {
						$balances[$participationd['guest_actor']]++;
						$left++;
					} else {
						break;
					}
				}
			}
		}

		$actors = array_keys($seen);
		sort($actors);
		$left = 0;
		foreach ($actors as $id) {
			$left += $balances[$id];
		}
		foreach ($actors as $id) {
			$p = $balances[$id] / $scm;
			if (!is_integer ($p)) {
				$p = round($p);
			}
			$balances[$id] = $p;
			$left = $left - $p * $scm;
		}
		while ($left != 0) {
			foreach ($actors as $id) {
				if ($left > 0) {
					$balances[$id] += 1;
					$left -= $scm;
				} elseif ($left < 0) {
					$balances[$id] -= 1;
					$left += $scm;
				} else {
					break;
				}
			}
		}

		$this->_cached_balance_data = $balances;
		asort($this->_cached_balance_data);

		return $this->_cached_balance_data;
	}

	function get_balances() {
		$data = $this->_get_balances_data();

		$balances = array();
		foreach ($data as $actor_id => $balance) {
			$actor = ObjectCache::retrieve_object('Actor', $actor_id);
			$actor->fetch_account();
			$balances[$actor_id] = array('actor' => $actor,
						     'balance' => amount_from_db($balance));
		}
		return $balances;
	}

	private function _get_suggestions_data() {
		if ($this->_cached_suggestion_data != NULL) {
			return $this->_cached_suggestion_data;
		}

		$bdata = $this->_get_balances_data();

		$cur = $bdata;
		$limiter = 100;
		$suggestions = array();
		$unbalance = 1;

		while (count($cur) > 0 && $limiter > 0 && $unbalance > 0) {
			$limiter--;

			$asc = $cur; asort($asc, SORT_NUMERIC);
			$desc = $cur; arsort($desc, SORT_NUMERIC);

			$asc_keys = array_keys($asc);
			$desc_keys = array_keys($desc);

			$min = $asc_keys[0];
			$max = $desc_keys[0];

			$min_balance = $cur[$min];
			$max_balance = $cur[$max];

			$suggested_amount = min($max_balance, -$min_balance);
			if ($suggested_amount == 0) {
				break;
			}
			$suggestions[] = array('from' => $min,
					       'to' => $max,
					       'amount' => $suggested_amount);
			// print "Suggestion: $suggested_amount de $min vers $max\n";
			$cur[$min] += $suggested_amount;
			$cur[$max] -= $suggested_amount;

			// Drop zeros
			$next = array();
			$unbalance = 0;
			foreach ($cur as $actor_id => $balance) {
				if ($balance == 0) {
					continue;
				} else {
					$next[$actor_id] = $balance;
				}
				$unbalance += abs($balance);
			}
			$cur = $next;
		}

		$this->_cached_suggestion_data = $suggestions;

		return $this->_cached_suggestion_data;
	}

	function get_suggestions() {
		$data = $this->_get_suggestions_data();

		$suggestions = array();
		foreach ($data as $s) {
			$suggestions[] = array('from' => ObjectCache::retrieve_object('Actor', $s['from']),
					       'to' => ObjectCache::retrieve_object('Actor', $s['to']),
					       'amount' => amount_from_db($s['amount']));
		}
		return $suggestions;
	}

	static function get_groups_from_cookies() {
		$group_cookies = get_group_cookies();

		$result = array();
		foreach ($group_cookies as $c) {
			$r = db_query('SELECT id FROM groups WHERE invitation_cookie=$1',
			      array($c));

			while ($row = pg_fetch_assoc($r)) {
				$result[] =& ObjectCache::retrieve_object('Group', $row['id']);
			}

		}
		return $result;
	}
}
