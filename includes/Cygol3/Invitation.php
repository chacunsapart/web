<?php

namespace Cygol3 ;

class Invitation {
	var $actor = NULL;
	var $invited_by = NULL;
	var $tstamp = 0;
	var $cookie = '';

	function __construct($cookie) {
		$this->load($cookie);
	}

	function load($cookie) {
		$r = db_query('SELECT * FROM invitations WHERE cookie=$1',
			      array($cookie));

		$result = array();
		if ($row = pg_fetch_assoc($r)) {
			$this->actor =& ObjectCache::retrieve_object('Actor', $row['actor_id']);
			$this->invited_by =& ObjectCache::retrieve_object('Account', $row['invited_by']);
			$this->tstamp = $row['tstamp'];
			$this->cookie = $row['cookie'];
		} else {
			throw new NotFoundException(_("No invitation with this cookie."));
		}
	}

	function delete() {
		$r = db_query('DELETE FROM invitations WHERE cookie=$1',
			      array($this->cookie));
		$this->actor = NULL;
		$this->invited_by = NULL;
		$this->tstamp = 0;
		$this->cookie = '';
	}

	function decline() {
		$this->delete();
	}

	function accept($account) {
		db_begin();

		$oldid = $this->actor->id;
		$new_actor =& $account->get_actor();
		$newid = $new_actor->id;

		if ($oldid != $newid) {
			$r = db_query('SELECT * FROM participations WHERE guest_actor=$1',
				      array($oldid));
			while ($row = pg_fetch_assoc($r)) {
				$r2 = db_query('SELECT * FROM participations WHERE guest_actor=$1 AND expense_id=$2',
					       array($newid, $row['expense_id']));
				if ($row2 = pg_fetch_assoc($r2)) {
					$r3 = db_query('UPDATE participations SET parts=$1 WHERE guest_actor=$2 AND expense_id=$3',
						       array($row['parts'] + $row2['parts'], $newid, $row['expense_id']));
					$r3 = db_query('DELETE FROM participations WHERE guest_actor=$1 AND expense_id=$2',
						       array($oldid, $row['expense_id']));
				} else {
					$r = db_query('UPDATE participations SET guest_actor=$1 WHERE guest_actor=$2',
						      array($newid, $oldid));
				}
			}

			$r = db_query('UPDATE expenses SET payer_actor=$1 WHERE payer_actor=$2',
				      array($newid, $oldid));
			$r = db_query('UPDATE groups SET creator_actor=$1 WHERE creator_actor=$2',
				      array($newid, $oldid));

			$old_actor = $this->actor;
			$this->delete();
			$old_actor->delete();
		} else {
			$this->delete();
		}
		db_commit();
	}
}
