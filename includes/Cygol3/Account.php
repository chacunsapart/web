<?php

namespace Cygol3 ;

class Account {
	var $id = 0;
	var $email = '';
	var $password_hash = '';
	var $auth_cookie = '';
	var $locale = '';
	var $actor = NULL;

	function __construct($id, $data=array()) {
		$this->load($id, $data);
	}

	function load($id, $data=array()) {
		if (count($data)) {
			$row = $data;
		} else {
			$r = db_query('SELECT * FROM accounts WHERE id=$1',
				      array($id));

			if (!$row = pg_fetch_assoc($r)) {
				throw new NotFoundException(_("No account with this id."));
			}
		}
		$this->id = $row['id'];
		$this->email = $row['email'];
		$this->password_hash = $row['password_hash'];
		$this->auth_cookie = $row['auth_cookie'];
		$this->locale = $row['locale'];
		$this->actor =& ObjectCache::retrieve_object('Actor', $row['actor_id']);
	}

	function save() {
		validate_email($this->email);
		validate_password_hash($this->password_hash);
		validate_auth_cookie($this->auth_cookie);
		validate_locale($this->locale);

		$r = db_query('UPDATE accounts SET email=$1, password_hash=$2, auth_cookie=$3, locale=$4 WHERE id=$5',
			      array($this->email, $this->password_hash, $this->auth_cookie, $this->locale, $this->id));
	}

	function get_actor() {
		return $this->actor;
	}

	function get_groups() {
		$r = db_query('SELECT DISTINCT group_id, group_tstamp FROM group_actor_vw WHERE acct_id=$1 ORDER BY group_tstamp DESC, group_id DESC',
			      array($this->id));

		$result = array();
		while ($row = pg_fetch_assoc($r)) {
			$result[] =& ObjectCache::retrieve_object('Group', $row['group_id']);
		}

		return $result;
	}

	function get_recently_active_friends() {
		$r = db_query('SELECT DISTINCT them.actor_id, them.group_id, them.group_tstamp FROM group_actor_vw them JOIN group_actor_vw me USING(group_id) WHERE me.acct_id=$1 ORDER BY them.group_tstamp DESC, them.group_id DESC',
			      array($this->id));

		$result = array();
		$lastgroup = -1;
		$lastfriends = array();
		$seenfriends = array();
		while ($row = pg_fetch_assoc($r)) {
			$gid = $row['group_id'];
			$fid = $row['actor_id'];
			if ($gid != $lastgroup) {
				usort($lastfriends, "Cygol3\cmp_actors_by_nick");
				$result = array_merge($result, $lastfriends);
				$lastfriends = array();
				$lastgroup = $gid;
			}
			if (!isset($seenfriends[$fid])) {
				$friend = ObjectCache::retrieve_object('Actor', $fid);
				$friend->fetch_account();
				$lastfriends[] = $friend;
				$seenfriends[$fid] = 1;
			}
		}
		usort($lastfriends, "Cygol3\cmp_actors_by_nick");
		$result = array_merge($result, $lastfriends);

		return $result;
	}
}
