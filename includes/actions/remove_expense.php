<?php

namespace Cygol3;

$expense =& ObjectCache::retrieve_object('Expense', getIntFromRequest('expense_id'));
check_and_redirect($expense->group);

$g = $expense->group;

setup_current_group($g);

try {
	$is_payback = $expense->is_payback;
	if ($is_payback) {
		export_view_param('active_tab', 'balances');
	}

	$expense->delete(getIntFromRequest('sure'));
}
catch (NotAllowedException $ex) {
	if (! $is_payback) {
		export_view_param('active_tab', 'options');
	}
	set_feedback(_("Cannot remove a non-empty expense."), "danger");
    export_view_param('expense', $expense);
	show_view("expense_details");
}

if ($is_payback) {
	set_feedback(_("Payback removed."), "success");
} else {
	set_feedback(_("Expense removed."), "success");
}

export_view_param('e', $g);
show_view("group_details");
