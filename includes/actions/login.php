<?php

namespace Cygol3;

try {
	$u = get_account_by_email_pass(getStringFromRequest('email'),
				       getStringFromRequest('password'));
}
catch (NotFoundException $ex) {
	$u = NULL;
}
setup_session($u);

$invitation_cookie = getStringFromRequest('accept_invitation_cookie');

if (! Cygol3::$logged_user) {
	set_feedback(_("Not logged in; wrong email or password."), "danger");

	show_view("home_notloggedin");
}

try {
  $i =& ObjectCache::retrieve_object('Invitation', $invitation_cookie);

  $i->accept(Cygol3::$logged_user);
}
catch (NotFoundException $ex) {
  // Not a problem
}

set_feedback(_("Logged in."), "success");

export_view_param('groups', Cygol3::$logged_user->get_groups());

show_view("home_loggedin");
