<?php

namespace Cygol3;

$g =& ObjectCache::retrieve_object('Group', getIntFromRequest('group_id'));
check_and_redirect($g);

setup_current_group($g);

$params = array();
$params['name'] = getStringFromRequest('name');
$params['amount'] = getFloatFromRequest('amount');
$params['group_id'] = $g->id;

try {
	db_begin();

	if (getIntFromRequest('payer_id') == 0) {
		$params['payer_actor_id'] = 0;
		$params['new_payer_nick'] = getStringFromRequest('new_actor_nick');
	} else {
		$params['payer_actor_id'] = getIntFromRequest('payer_id');

	}

	$clone_tx = NULL;
	if (getIntFromRequest('cloned_expense_id')) {
		$expense =& ObjectCache::retrieve_object('Expense', getIntFromRequest('cloned_expense_id'));
		if ($expense->group->id == $g->id) {
			$clone_tx =& $expense;
		}
	} else {
		$expenses = $g->get_expenses();
		foreach ($expenses as $tx) {
			if (! $tx->is_payback) {
				$clone_tx = $tx;
				break;
			}
		}
	}

	if ($clone_tx) {
		$guests = array();
		foreach ($clone_tx->get_participations() as $px) {
			$guests[] = array('guest_actor_id' => $px->guest->id,
					  'nb_parts' => $px->parts);
		}
		$params['guests'] = $guests;
	} else {
		$params['guests'] = array(array('guest_actor_id' => $params['payer_actor_id'],
						'nb_parts' => $g->fraction));
	}

	$expense =& create_expense($params);
	db_commit();

	set_feedback(_("Expense created.  You should now check the guests and the numbers of parts."), "success");

	export_view_param('expense', $expense);
	show_view("expense_details");
}
catch (BadAmountException $ex) {
	db_rollback();
	set_feedback($ex->getMessage(), "danger");

	export_view_param('e', $g);
	show_view("group_details");
}
catch (EmptyValueException $ex) {
	db_rollback();
	set_feedback($ex->getMessage(), "danger");

	export_view_param('e', $g);
	show_view("group_details");
}
catch (TooLongException $ex) {
	db_rollback();
	set_feedback($ex->getMessage(), "danger");

	export_view_param('e', $g);
	show_view("group_details");
}
catch (NotAllowedException $ex) {
	db_rollback();
	set_feedback($ex->getMessage(), "danger");

	export_view_param('e', $g);
	show_view("group_details");
}
