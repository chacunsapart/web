<?php

namespace Cygol3;

$invited =& ObjectCache::retrieve_object('Actor', getIntFromRequest('actor_id'));
$from_group =& ObjectCache::retrieve_object('Group', getIntFromRequest('from_group_id'));

check_and_redirect($from_group);

$participants = $from_group->get_participants();

$found = false;
foreach ($participants as $f) {
	if ($f->id == $invited->id) {
		$found = true;

		$params = array('invited_by' => Cygol3::$logged_user->id,
				'actor_id' => $invited->id);

		$invitation =& create_invitation($params);

		set_feedback(_("Invitation created.  It will be valid for one week."), "success");

		break;
	}
}

if ($found) {
	export_view_param('invitation', $invitation);
	export_view_param('from_group', $from_group);
	show_view("invite");
} else {
	show_view("home_loggedin");
}
