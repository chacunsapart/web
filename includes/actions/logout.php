<?php

namespace Cygol3;

setup_session(NULL);
set_cookie('group_cookie', '', 0);
Cygol3::$cookies['group_cookie'] = '';

set_feedback(_("Logged out."), "success");

show_view("home_notloggedin");
