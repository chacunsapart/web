<?php

namespace Cygol3;

export_view_param('stats', gather_stats());

expire_expirable_maybe(0.01);

show_view("about");
