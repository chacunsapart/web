<?php

namespace Cygol3;

try {
	$u = get_account_by_email_pass(Cygol3::$logged_user->email,
				       getStringFromRequest('oldpassword'));
}
catch (NotFoundException $ex) {
	set_feedback(_("Wrong password."), "danger");

	export_view_param('active_tab', 'password');
	show_view("my_account");
}

$pw1 = getStringFromRequest('newpassword1');
$pw2 = getStringFromRequest('newpassword2');

if ($pw1 != $pw2) {
	set_feedback(_("Mismatched passwords."), "danger");

	export_view_param('active_tab', 'password');
	show_view("my_account");
}
if (trim($pw1) == "") {
	export_view_param('active_tab', 'password');
	show_view("my_account");
}

try {
	db_begin();
	Cygol3::$logged_user->password_hash = encode_password($pw1);
	Cygol3::$logged_user->save();
	db_commit();
}
catch (EmptyValueException $ex) {
	db_rollback();

	set_feedback(_("Empty password."), "danger");

	export_view_param('active_tab', 'password');
	show_view("my_account");
}
catch (TooLongException $ex) {
	db_rollback();

	set_feedback(_("Password too long."), "danger");

	export_view_param('active_tab', 'password');
	show_view("my_account");
}

set_feedback(_("Password updated."), "success");

export_view_param('active_tab', 'password');
show_view("my_account");
