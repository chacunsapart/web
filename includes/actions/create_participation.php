<?php

namespace Cygol3;

$expense =& ObjectCache::retrieve_object('Expense', getIntFromRequest('expense_id'));
check_and_redirect($expense->group);

$params = array();
$params['expense_id'] = $expense->id;

$parts = getIntFromRequest('parts');

if (getIntFromRequest('guest_id') == 0) {
	$params['guest_actor_id'] = 0;
	$params['new_guest_nick'] = getStringFromRequest('new_actor_nick');
} else {
	$params['guest_actor_id'] = getIntFromRequest('guest_id');
}
$params['parts'] = $parts;

try {
	$participation =& create_participation($params);

	if ($participation->parts == $parts) {
		set_feedback(_("Guest added."), "success");
	} elseif ($parts > 0) {
		set_feedback(_("New parts added."), "success");
	} else {
		set_feedback(_("Guest parts removed."), "success");
	}
}
catch (NotAllowedException $ex) {
	set_feedback($ex->getMessage(), "danger");
}

export_view_param('expense', $expense);
show_view("expense_details");
