<?php

namespace Cygol3;

$invitation_cookie = getStringFromRequest('c');

$yes = getIntFromRequest('yes');
$no = getIntFromRequest('no');

try {
	$invitation =& ObjectCache::retrieve_object('Invitation', $invitation_cookie);
}
catch (NotFoundException $ex) {
	set_feedback(_("Invitation not found (may have expired)."), "danger");

	if (Cygol3::$logged_user) {
		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}
}

export_view_param('inviter', $invitation->invited_by->get_actor());

if ($no) {
	$invitation->decline();

	set_feedback(_("Invitation declined."), "success");

	if (Cygol3::$logged_user) {
		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}
} elseif ($yes && Cygol3::$logged_user) {
	$invitation->accept(Cygol3::$logged_user);

	set_feedback(_("Invitation accepted."), "success");

	show_view("home_loggedin");
} elseif (Cygol3::$logged_user) {
	export_view_param('invitation', $invitation);
	show_view("accept_loggedin");
} else {
	export_view_param('invitation', $invitation);
	show_view("accept_notloggedin");
}
