<?php

namespace Cygol3;

$participation =& ObjectCache::retrieve_object('Participation', getIntFromRequest('participation_id'));
check_and_redirect($participation->expense->group);

$expense = $participation->expense;
$participation->delete();

set_feedback(_("Guest removed."), "success");

export_view_param('expense', $expense);
show_view("expense_details");
