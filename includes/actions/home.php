<?php

namespace Cygol3;

if (Cygol3::$logged_user) {
	show_view("home_loggedin");
} else {
	show_view("home_notloggedin");
}
