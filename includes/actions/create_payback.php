<?php

namespace Cygol3;

$g =& ObjectCache::retrieve_object('Group', getIntFromRequest('group_id'));
check_and_redirect($g);

setup_current_group($g);

$params['from'] = getIntFromRequest('payer_id');
$params['to'] = getIntFromRequest('payee_id');
$params['amount'] = getFloatFromRequest('amount');
$params['name'] = getStringFromRequest('comment');

try {
	$expense =& create_expense(array('group_id' => $g->id,
					 'is_payback' => 1,
					 'payer_actor_id' => $params['from'],
					 'amount' => $params['amount'],
					 'name' => $params['name'],
					 'guests' => array(array('guest_actor_id' => $params['to'],
								 'nb_parts' => 1))));

	set_feedback(_("Payback created."), "success");
}
catch (BadAmountException $ex) {
	set_feedback($ex->getMessage(), "danger");
}
catch (EmptyValueException $ex) {
	set_feedback($ex->getMessage(), "danger");
}
catch (TooLongException $ex) {
	set_feedback($ex->getMessage(), "danger");
}

export_view_param('active_tab', 'balances');

export_view_param('e', $g);
show_view("group_details");
