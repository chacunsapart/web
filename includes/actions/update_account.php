<?php

namespace Cygol3;

try {
	$u = get_account_by_email_pass(Cygol3::$logged_user->email,
				       getStringFromRequest('password'));
}
catch (NotFoundException $ex) {
	set_feedback(_("Wrong password."), "danger");

	show_view("my_account");
}

Cygol3::$logged_user->email = getStringFromRequest('email');
Cygol3::$logged_user->locale = canonicalize_locale(getStringFromRequest('locale'), Cygol3::$config['known_languages']);
$actor =& Cygol3::$logged_user->get_actor();
$actor->nick = getStringFromRequest('nick');

try {
	db_begin();

	$actor->save();
	Cygol3::$logged_user->save();

	db_commit();

	setup_session($u);
	set_feedback(_("Account updated."), "success");
}
catch (EmptyValueException $ex) {
	db_rollback();

	Cygol3::$logged_user->load(Cygol3::$logged_user->id);
	$actor->load($actor->id);

	set_feedback(_("Empty name, email or password."), "danger");
}
catch (TooLongException $ex) {
	db_rollback();

	Cygol3::$logged_user->load(Cygol3::$logged_user->id);
	$actor->load($actor->id);

	set_feedback(_("Name, email or password too long."), "danger");
}
catch (InvalidValueException $ex) {
	db_rollback();

	Cygol3::$logged_user->load(Cygol3::$logged_user->id);
	$actor->load($actor->id);

	set_feedback(_("Invalid email."), "danger");
}

setup_session($u);

show_view("my_account");
