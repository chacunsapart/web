<?php

namespace Cygol3;

$email = getStringFromRequest('email');

try {
	$u = get_account_by_email($email);
}
catch (NotFoundException $ex) {
	set_feedback(_("No account with that email."), "danger");

	show_view("home_notloggedin");
}


$code = create_recovery_code($u);

$email_smarty = get_smarty_object();
$email_smarty->assign("name", $u->get_actor()->nick);
$email_smarty->assign("email", $u->email);
$email_smarty->assign("recoverycode", $code);
$email_smarty->assign('sn', Cygol3::$config['sn']);
$email_smarty->assign('prefix', Cygol3::$config['prefix']);
$mail_subj = $email_smarty->fetch("../templates/recovery_email_subject.tpl");
$mail_subj = mb_encode_mimeheader($mail_subj, 'UTF-8', 'Q');
$mail_body = $email_smarty->fetch("../templates/recovery_email_body.tpl");

if (mail($u->email, $mail_subj, $mail_body, NULL, "-f".Cygol3::$config['contactemail'])) {
	set_feedback(_("An email has been sent, with a recovery code.  Please check your inbox, then paste the recovery code below to change your password and recover your account.  The recovery code will be valid for one week."), "warning");
} else {
	set_feedback(_("An error happened when sending the recovery email."), "danger");
}

export_view_param('email', $email);
export_view_param('recoverycode', '');

show_view("account_recovery_form");
