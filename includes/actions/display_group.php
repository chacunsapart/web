<?php

namespace Cygol3;

try {
	$g =& ObjectCache::retrieve_object('Group', getIntFromRequest('group_id'));
}
catch (NotFoundException $ex) {
	set_feedback(_("No such group; may have been deleted."), "danger");

	if (Cygol3::$logged_user) {
		export_view_param('groups', Cygol3::$logged_user->get_groups());

		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}
}

if (getIntFromRequest('all_expenses')) {
	export_view_param('show_all', 1);
} elseif (getIntFromRequest('all_paybacks')) {
	export_view_param('show_all', 1);
	export_view_param('active_tab', 'balances');
} elseif (getIntFromRequest('show_balances')) {
	export_view_param('active_tab', 'balances');
} else {
	export_view_param('show_all', 0);
}

check_and_redirect($g);

setup_current_group($g);

export_view_param('e', $g);
show_view("group_details");
