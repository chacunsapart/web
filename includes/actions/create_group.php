<?php

namespace Cygol3;

$params = array();
$params['group_name'] = getStringFromRequest('name');
$params['group_fraction'] = 1;
$params['locale'] = Cygol3::$lang;
try {
	db_begin();
	if (Cygol3::$logged_user) {
		$params['creator_actor_id'] = Cygol3::$current_actor->id;
	} else {
		$a =& create_actor(array('nick' => getStringFromRequest('creator_name')));
		$params['creator_actor_id'] = $a->id;
	}
	$e =& create_group($params);
	db_commit();
}
catch (EmptyValueException $ex) {
	db_rollback();
	set_feedback(_("Empty name."), "danger");

	if (Cygol3::$logged_user) {
		export_view_param('groups', Cygol3::$logged_user->get_groups());
		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}

}
catch (TooLongException $ex) {
	db_rollback();
	set_feedback(_("Name too long."), "danger");

	if (Cygol3::$logged_user) {
		export_view_param('groups', Cygol3::$logged_user->get_groups());
		show_view("home_loggedin");
	} else {
		show_view("home_notloggedin");
	}
}

export_view_param('e', $e);
export_view_param('params', $params);
set_feedback(_("Group created, now add some expenses."), "success");

show_view("group_details");
