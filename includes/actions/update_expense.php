<?php

namespace Cygol3;

$expense =& ObjectCache::retrieve_object('Expense', getIntFromRequest('expense_id'));
check_and_redirect($expense->group);

try {
	setup_current_group($expense->group);

	db_begin();

	$expense->name = getStringFromRequest('name');
	$expense->amount = getFloatFromRequest('amount');
	if (getIntFromRequest('payer_id') == 0) {
		$a = create_actor(array('nick' => getStringFromRequest('new_actor_nick')));
		$expense->payer = $a;
	} else {
		if (!check_actor_is_known(getIntFromRequest('payer_id'))) {
			throw new NotAllowedException(_("Payer id not in your list of friends."));
		}

		$expense->payer =& ObjectCache::retrieve_object('Actor', getIntFromRequest('payer_id'));
	}
	$expense->save();

	db_commit();

	set_feedback(_("Expense updated."), "success");
}
catch (BadAmountException $ex) {
	db_rollback();

	$expense->load($expense->id);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}
catch (EmptyValueException $ex) {
	db_rollback();

	$expense->load($expense->id);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}
catch (TooLongException $ex) {
	db_rollback();

	$expense->load($expense->id);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}
catch (NotAllowedException $ex) {
	db_rollback();

	$expense->load($expense->id);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}

export_view_param('expense', $expense);
show_view("expense_details");
