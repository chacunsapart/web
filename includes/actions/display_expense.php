<?php

namespace Cygol3;

$expense =& ObjectCache::retrieve_object('Expense', getIntFromRequest('expense_id'));

check_and_redirect($expense->group);

setup_current_group($expense->group);

export_view_param('expense', $expense);
show_view("expense_details");
