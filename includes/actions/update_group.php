<?php

namespace Cygol3;

$g =& ObjectCache::retrieve_object('Group', getIntFromRequest('group_id'));
check_and_redirect($g);

$g->name = getStringFromRequest('name');
$g->locale = canonicalize_locale(getStringFromRequest('locale'), Cygol3::$config['known_regions']);
$g->fraction = getIntFromRequest('fraction', 1);

try {
	db_begin();

	$g->save();

	db_commit();

	export_view_param('e', $g);

	set_feedback(_("Group updated."), "success");
}
catch (EmptyValueException $ex) {
	db_rollback();

	$g->load($g->id);

	export_view_param('e', $g);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}
catch (TooLongException $ex) {
	db_rollback();

	$g->load($g->id);

	export_view_param('e', $g);
	set_feedback($ex->getMessage(), "danger");

	export_view_param('active_tab', 'options');
}

setup_current_group($g);

show_view("group_details");
