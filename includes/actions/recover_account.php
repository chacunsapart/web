<?php

namespace Cygol3;

$email = getStringFromRequest('email');
$recoverycode = getStringFromRequest('recoverycode');
$pw1 = getStringFromRequest('newpassword1');
$pw2 = getStringFromRequest('newpassword2');

try {
	$u = get_account_by_email($email);
}
catch (NotFoundException $ex) {
	set_feedback(_("No account with that email."), "danger");

	show_view("home_notloggedin");
}

try {
	$check = check_recovery_code($u, $recoverycode);
}
catch (NotFoundException $ex) {
	set_feedback($ex->getMessage(), "danger");

	show_view("home_notloggedin");
}

export_view_param('email', $email);

if (! $check) {
	set_feedback(_("Wrong recovery code."), "danger");

	export_view_param('recoverycode', '');
	show_view("account_recovery_form");
}

export_view_param('recoverycode', $recoverycode);

if ($pw1 != $pw2) {
	set_feedback(_("Mismatched passwords."), "danger");

	show_view("account_recovery_form");
}

try {
	db_begin();
	$u->password_hash = encode_password($pw1);
	$u->save();
	delete_recovery_code($u);
	db_commit();
}
catch (EmptyValueException $ex) {
	db_rollback();

	set_feedback(_("Please type in a password."), "warning");


	show_view("account_recovery_form");
}
catch (TooLongException $ex) {
	db_rollback();

	set_feedback(_("Password too long."), "warning");


	show_view("account_recovery_form");
}

set_feedback(_("Password changed."), "success");

show_view("home_notloggedin");
