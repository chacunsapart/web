<?php

namespace Cygol3;

$params = array();
$params['nick'] = getStringFromRequest('nick');
$params['email'] = getStringFromRequest('email');
$params['password'] = getStringFromRequest('password');

$invitation_cookie = getStringFromRequest('accept_invitation_cookie');
try {
	$i =& ObjectCache::retrieve_object('Invitation', $invitation_cookie);

	$params['actor_id'] = $i->actor->id;
}
catch (NotFoundException $ex) {
	$i = NULL;
}

try {
	db_begin();

	$u =& create_account($params);
}
catch (DuplicateException $ex) {
	db_rollback();

	setup_session(NULL);

	set_feedback(_("An account already exists for that email address."), "danger");

	show_view("home_notloggedin");
}
catch (EmptyValueException $ex) {
	db_rollback();

	setup_session(NULL);

	set_feedback(_("Empty name, email or password."), "danger");

	show_view("home_notloggedin");
}
catch (TooLongException $ex) {
	db_rollback();

	setup_session(NULL);

	set_feedback(_("Name, email or password too long."), "danger");

	show_view("home_notloggedin");
}

setup_session($u);

if ($i != NULL) {
	$i->accept(Cygol3::$logged_user);

	set_feedback(_("Invitation accepted."), "success");
} else {
	set_feedback(_("Account created successfully."), "success");
}

db_commit();

show_view("home_loggedin");
