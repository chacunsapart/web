<?php

namespace Cygol3;

function process_known_locales() {
	$lbl = array();
	$lbr = array();

	foreach (Cygol3::$config['known_languages'] as $l) {
		$l = $l.'.UTF-8';
		$lang = locale_get_primary_language($l);
		$region = locale_get_region($l);
		if (!isset($lbl[$lang])) {
			$lbl[$lang] = array();
		}
		$lbl[$lang][] = array('region' => $region, 'locale' => $l);
	}

	foreach (Cygol3::$config['known_regions'] as $l) {
		$l = $l.'.UTF-8';
		$lang = locale_get_primary_language($l);
		$region = locale_get_region($l);
		if (!isset($lbr[$region])) {
			$lbr[$region] = array();
		}

		$lbr[$region][] = array('lang' => $lang, 'locale' => $l);
	}

	$langs = array();
	foreach ($lbl as $l => $v) {
		$langs[$l] = format_locale_getlang($v[0]['locale']);
	}
	asort ($langs, SORT_LOCALE_STRING);

	$regions = array();
	foreach ($lbr as $r => $v) {
		$regions[$r] = format_locale_getregion($v[0]['locale']);
	}
	asort ($regions, SORT_LOCALE_STRING);

	Cygol3::$locales_by_lang = array();
	Cygol3::$locales_by_region = array();
	foreach ($langs as $l => $v) {
		$r2 = array();
		foreach ($lbl[$l] as $r => $v2) {
			$r2[$r] = format_locale_getregion($v2['locale']);
		}
		asort ($r2, SORT_LOCALE_STRING);
		foreach ($r2 as $r => $v2) {
			Cygol3::$locales_by_lang[$l][$r] = $lbl[$l][$r];
		}
	}
	foreach ($regions as $r => $v) {
		$l2 = array();
		foreach ($lbr[$r] as $l => $v2) {
			$l2[$l] = format_locale_getlang($v2['locale']);
		}
		asort ($l2, SORT_LOCALE_STRING);
		foreach ($l2 as $l => $v2) {
			Cygol3::$locales_by_region[$r][$l] = $lbr[$r][$l];
		}
	}
}

process_known_locales();

function canonicalize_locale($locale, $list) {
	$res = locale_lookup ($list, $locale, true);
	if (!$res) {
		$lang = locale_get_primary_language(locale_canonicalize($locale));
		foreach (Cygol3::$config['main_locales'] as $l) {
			$l = locale_canonicalize($l);
			if (preg_match("/^${lang}_/", $l)) {
				$res = $l;
				break;
			}
		}
		if (!$res) {
			$res = Cygol3::$config['main_locales'][0];
		}
	}
	$res = locale_lookup ($list, $res, false);
	$res .= '.UTF-8';
	return $res;
}

function set_language($locale) {
	Cygol3::$lang = canonicalize_locale($locale, Cygol3::$config['known_languages']);

	setlocale (LC_ALL, Cygol3::$lang);
}

function format_locale_getlang($locale) {
	return locale_get_display_language($locale, Cygol3::$lang);
}

function format_locale_getregion($locale) {
	return locale_get_display_region($locale, Cygol3::$lang);
}

function format_locale($locale) {
	return locale_get_display_name($locale, Cygol3::$lang);
}

textdomain('chacunsapart');
bindtextdomain('chacunsapart', $_SERVER['DOCUMENT_ROOT'].'/../locales/');
