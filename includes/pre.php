<?php

namespace Cygol3;

require('exceptions.php');
require('cygol3.php');

foreach (array('Group','Actor','Account','Invitation','Expense','Participation') as $c) {
	require "Cygol3/$c.php";
}

require('config.php');

require('db.php');
require('utils.php');
require('gettext.php');
require('smarty.php');
require('auth.php');

db_connect(array('db_host' => Cygol3::$config['db_host'],
		 'db_name' => Cygol3::$config['db_name'],
		 'db_user' => Cygol3::$config['db_user'],
		 'db_pass' => Cygol3::$config['db_pass']));

do_auth();
