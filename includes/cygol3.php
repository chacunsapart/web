<?php

namespace Cygol3 ;

function validate_email($email) {
	$tmp = trim($email);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty email."));
	}
	if (strlen($tmp) > 100) {
		throw new TooLongException(_("Email too long."));
	}

	require_once 'Validate.php';

	if (!\Validate::email($tmp, array('use_rfc822' => true))) {
		throw new InvalidValueException(_("This doesn't look like an email address."));
	}
}

function validate_password($password) {
	$tmp = trim($password);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty password."));
	}
	if (strlen($tmp) > 100) {
		throw new TooLongException(_("Password too long."));
	}
}

function validate_password_hash($password_hash) {
	$tmp = trim($password_hash);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty password hash."));
	}
}

function validate_auth_cookie($auth_cookie) {
	$tmp = trim($auth_cookie);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty auth cookie."));
	}
}

function validate_locale($locale) {
	$tmp = trim($locale);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty locale."));
	}
}

function validate_nick($nick) {
	$tmp = trim($nick);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty nick."));
	}
	if (strlen($tmp) > 100) {
		throw new TooLongException(_("Nick too long."));
	}
}

function validate_group_name($group_name) {
	$tmp = trim($group_name);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty group name."));
	}
	if (strlen($tmp) > 100) {
		throw new TooLongException(_("Group name too long."));
	}
}

function validate_expense_name($expense_name) {
	$tmp = trim($expense_name);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty expense name."));
	}
	if (strlen($tmp) > 100) {
		throw new TooLongException(_("Expense name too long."));
	}
}

function validate_group_cookie($group_cookie) {
	$tmp = trim($group_cookie);
	if ($tmp == '') {
		throw new EmptyValueException(_("Empty invitation cookie."));
	}
}

function validate_group_fraction($fraction) {
	if ($fraction == 0) {
		throw new InvalidValueException(_("Fraction can't be zero."));
	}
	if ($fraction > 4) {
		throw new InvalidValueException(_("Fraction is too large."));
	}
}


function create_account($params) {
	validate_email($params['email']);
	validate_password($params['password']);

	db_begin();

	if (isset($params['actor_id'])) {
		$actor = ObjectCache::retrieve_object('Actor', $params['actor_id']);
	} else {
		$actor = create_actor(array('nick' => $params['nick']));
	}

	try {
		$r = db_query('SELECT id FROM accounts WHERE email=$1',
			      array($params['email']));
		if (pg_fetch_row($r)) {
			db_rollback();
			throw new DuplicateException (_("An account with that email already exists."));
		}

		$r = db_query("SELECT nextval('accounts_id_seq')");
		$row = pg_fetch_row($r);
		$newid = $row[0];

		$auth_cookie = md5(util_randbytes());

		$dbp = array($newid, $params['email'], encode_password($params['password']), $auth_cookie, $actor->id);

		$r = db_query('INSERT INTO accounts (id, email, password_hash, auth_cookie, actor_id) VALUES ($1, $2, $3, $4, $5)',
			      $dbp);
	}
	catch (\DBException $e) {
		db_rollback();
		throw new DuplicateException ($e);
	}

	$result = ObjectCache::retrieve_object('Account', $newid);

	db_commit();

	return $result;
}

function get_account_by_auth_cookie($cookie) {
	$r = db_query('SELECT id FROM accounts WHERE auth_cookie=$1',
		      array($cookie));

	if ($row = pg_fetch_assoc($r)) {
		return ObjectCache::retrieve_object('Account', $row['id']);
	} else {
		throw new NotFoundException(_("No account with this cookie."));
	}
	return $result;
}

function get_account_by_email($email) {
	$r = db_query('SELECT id FROM accounts WHERE email=$1',
		      array($email));

	if ($row = pg_fetch_assoc($r)) {
		return ObjectCache::retrieve_object('Account', $row['id']);
	} else {
		throw new NotFoundException(_("No account with this email."));
	}
}

function get_account_by_email_pass($email, $password) {
	$r = db_query('SELECT id, password_hash FROM accounts WHERE email=$1',
		      array($email));

	if ($row = pg_fetch_assoc($r)) {
		if (check_password($password,$row['password_hash'])) {
			return ObjectCache::retrieve_object('Account', $row['id']);
		} else {
			throw new NotFoundException(_("No account with this email/password."));
		}
	} else {
		throw new NotFoundException(_("No account with this email/password."));
	}

	return $result;
}

function expire_old_recovery_codes() {
	$max_duration = 86400 * 7;

	$r = db_query('DELETE FROM account_recovery_codes WHERE tstamp < $1',
		      array(time() - $max_duration));
}

function create_recovery_code($account) {
	expire_old_recovery_codes();

	db_begin();

	$r = db_query('DELETE FROM account_recovery_codes WHERE acct_id=$1',
		      array($account->id));

	$recoverycode = md5(util_randbytes());

	$r = db_query('INSERT INTO account_recovery_codes (acct_id, code_hash, tstamp) VALUES ($1, $2, $3)',
		      array($account->id, encode_password($recoverycode), time()));

	db_commit();

	return $recoverycode;
}

function check_recovery_code($account, $code) {
	expire_old_recovery_codes();

	$r = db_query('SELECT * FROM account_recovery_codes WHERE acct_id=$1',
		      array($account->id));

	if ($row = pg_fetch_assoc($r)) {
		if (check_password($code,$row['code_hash'])) {
			return true;
		} else {
			return false;
		}
	} else {
		throw new NotFoundException(_("No recovery code for this account; may have expired."));
	}
}

function delete_recovery_code($account) {
	$r = db_query('DELETE FROM account_recovery_codes WHERE acct_id=$1',
		      array($account->id));
}

function create_actor($params) {
	validate_nick($params['nick']);

	db_begin();

	$r = db_query("SELECT nextval('actors_id_seq')");
	$row = pg_fetch_row($r);
	$newid = $row[0];

	$dbp = array($newid, $params['nick']);

	$r = db_query('INSERT INTO actors (id, nick) VALUES ($1, $2)',
		      $dbp);

	$result = ObjectCache::retrieve_object('Actor', $newid);

	db_commit();

	return $result;
}

function expire_old_invitations() {
	$max_duration = 86400 * 7;

	$r = db_query('DELETE FROM invitations WHERE tstamp < $1',
		      array(time() - $max_duration));
}

function create_invitation($params) {
	db_begin();

	expire_old_invitations();

	$r = db_query('SELECT cookie FROM invitations WHERE actor_id=$1 AND invited_by=$2',
		      array($params['actor_id'], $params['invited_by']));
	$invitation_cookie = '';
	if ($row = pg_fetch_assoc($r)) {
		$invitation_cookie = $row['cookie'];
		$r = db_query('UPDATE invitations SET tstamp=$1 WHERE cookie=$2',
			      array(time(), $invitation_cookie));
	} else {
		$invitation_cookie = md5(util_randbytes());

		$r = db_query('INSERT INTO invitations (cookie, actor_id, invited_by, tstamp) VALUES ($1, $2, $3, $4)',
			      array($invitation_cookie, $params['actor_id'], $params['invited_by'], time()));
	}

	$result = ObjectCache::retrieve_object('Invitation', $invitation_cookie);

	db_commit();

	return $result;
}

function create_group($params) {
	validate_group_name($params['group_name']);

	db_begin();

	$r = db_query("SELECT nextval('groups_id_seq')");
	$row = pg_fetch_row($r);
	$newid = $row[0];

	$invitation_cookie = md5(util_randbytes());

	$dbp = array($newid, $params['group_name'], $params['creator_actor_id'], time(), 1, $invitation_cookie, $params['group_fraction']);

	$r = db_query('INSERT INTO groups (id, name, creator_actor, tstamp, status, invitation_cookie, fraction) VALUES ($1, $2, $3, $4, $5, $6, $7)',
		      $dbp);
	$result = ObjectCache::retrieve_object('Group', $newid);

	db_commit();

	return $result;
}

function create_expense($params) {
	validate_expense_name($params['name']);

	if ($params['amount'] <= 0) {
		throw new BadAmountException(_("Amounts must be positive."));
	}
	if (!isset($params['group_id']) || $params['group_id'] <= 0) {
		throw new EmptyValueException(_("No group."));
	}
	if (!isset($params['payer_actor_id']) || ($params['payer_actor_id'] <= 0 && !isset($params['new_payer_nick']))) {
		throw new EmptyValueException(_("No payer."));
	}

	if ($params['payer_actor_id'] == 0) {
		$a = create_actor(array('nick' => $params['new_payer_nick']));
		$params['payer_actor_id'] = $a->id;
		
	} elseif (!check_actor_is_known($params['payer_actor_id'])) {
		throw new NotAllowedException(_("Payer id not in your list of friends."));
	}

	db_begin();

	if (! isset($params['is_payback'])) {
		$params['is_payback'] = 0;
	}

	if ($params['is_payback']) {
		$g = ObjectCache::retrieve_object('Group', $params['group_id']);
		$from_allowed = false;
		foreach ($g->get_participants() as $f) {
			if ($f->id == $params['payer_actor_id']) {
				$from_allowed = true;
				break;
			}
		}
		if (!$from_allowed) {
			throw new NotAllowedException(_("Payer of the payback not in the group."));
		}

		if (count($params['guests']) != 1) {
			throw new NotAllowedException(_("Payback expenses can have only one guest."));
		}

		$r = $params['guests'][0];
		$to_allowed = false;
		foreach ($g->get_participants() as $f) {
			if ($f->id == $r['guest_actor_id']) {
				$to_allowed = true;
				break;
			}
		}
		if (!$to_allowed) {
			throw new NotAllowedException(_("Recipient of the payback not in the group."));
		}
	} else {
 		foreach ($params['guests'] as $g) {
			if ($g['guest_actor_id'] != 0
			    && !check_actor_is_known($g['guest_actor_id'])) {
				throw new NotAllowedException(_("Payer id not in your list of friends."));
			}
		}
	}

	$r = db_query("SELECT nextval('expenses_id_seq')");
	$row = pg_fetch_row($r);
	$newid = $row[0];

	$dbp = array($newid, $params['group_id'], $params['payer_actor_id'], amount_to_db($params['amount']), $params['name'], $params['is_payback'], time());

	$r = db_query('INSERT INTO expenses (id, group_id, payer_actor, amount, name, is_payback, tstamp) VALUES ($1, $2, $3, $4, $5, $6, $7)',
		      $dbp);
	$expense_id = $newid;

	if (isset($params['guests'])) {
 		foreach ($params['guests'] as $g) {
			$gi = $g['guest_actor_id'];
			if ($gi == 0) {
				$gi = $params['payer_actor_id'];
			}
			create_participation(array('expense_id' => $expense_id,
						   'guest_actor_id' => $gi,
						   'parts' => $g['nb_parts']));
		}
	}

	$expense = ObjectCache::retrieve_object('Expense', $newid);
	$expense->group->clear_cache();
	$expense->group->save();

	db_commit();

	return $expense;
}

function create_participation($params) {
	db_begin();

	if ($params['guest_actor_id'] == 0) {
		$a = create_actor(array('nick' => $params['new_guest_nick']));
		$params['guest_actor_id'] = $a->id;
	} elseif (!check_actor_is_known($params['guest_actor_id'])) {
		throw new NotAllowedException(_("Guest id not in your list of friends."));
	}

	$r = db_query('SELECT id, parts FROM participations WHERE expense_id=$1 AND guest_actor=$2',
		      array($params['expense_id'],
			    $params['guest_actor_id']));
	if ($row = pg_fetch_assoc($r)) {
		if (isset($params['absolute_parts']) && $params['absolute_parts']) {
			$new_parts = $params['parts'];
		} else {
			$new_parts = $row['parts'] + $params['parts'];
		}

		if ($new_parts <= 0) {
			throw new NotAllowedException(_("Can't have negative (or zero) parts."));
		}

		$r = db_query('UPDATE participations SET parts=$1, tstamp=$2 WHERE expense_id=$3 AND guest_actor=$4',
			      array($new_parts,
				    time(),
				    $params['expense_id'],
				    $params['guest_actor_id']));

		$participation = ObjectCache::refresh_object('Participation', $row['id']);
	} else {
		$r = db_query("SELECT nextval('participations_id_seq')");
		$row = pg_fetch_row($r);
		$newid = $row[0];

		if ($params['parts'] <= 0) {
			throw new NotAllowedException(_("Can't have negative (or zero) parts."));
		}

		$r = db_query('INSERT INTO participations (id, expense_id, tstamp, guest_actor, parts) VALUES ($1, $2, $3, $4, $5)',
			      array($newid,
				    $params['expense_id'],
				    time(),
				    $params['guest_actor_id'],
				    $params['parts']));

		$participation = ObjectCache::retrieve_object('Participation', $newid);
	}

	$participation->expense->group->clear_cache();
	$participation->expense->save();

	db_commit();

	return $participation;
}


function get_group_cookies() {
	$potential_cookies = explode('/', getStringFromCookie('group_cookie'));
	$potential_cookies[] = getStringFromRequest('group_cookie');

	$cookies = preg_grep('/^[0-9a-f]{32}$/', $potential_cookies);
	$result = array();
	$seen = array();
	foreach ($cookies as $c) {
		if (!isset ($seen[$c])) {
			$seen[$c] = 1;
			$result[] = $c;
		}
	}
	return $result;
}

function get_all_available_groups() {
	$gl = Group::get_groups_from_cookies();
	if (Cygol3::$logged_user) {
		$gl2 = Cygol3::$logged_user->get_groups();
		$groups = merge_group_list($gl, $gl2);
		$gl = $groups;
	}
	return $gl;
}

function check_actor_is_known($actor_id) {
	if (Cygol3::$logged_user) {
		$friends = Cygol3::$logged_user->get_recently_active_friends();
		foreach (Cygol3::$logged_user->get_recently_active_friends() as $f) {
			if ($f->id == $actor_id) {
				return true;
			}
		}
	}
	foreach (get_all_available_groups() as $g) {
		foreach ($g->get_participants() as $f) {
			if ($f->id == $actor_id) {
				return true;
			}
		}
	}
	return false;
}

function get_all_friends() {
	$fl = array();
	if (Cygol3::$logged_user) {
		$fl = merge_user_list($fl, Cygol3::$logged_user->get_recently_active_friends());
	}
	foreach (get_all_available_groups() as $g) {
		$fl = merge_user_list($fl, $g->get_participants());
	}
	return $fl;
}

class ObjectCache {
	private static $_cache;
	private static $_instance;

	public static function getInstance() {
		if (!isset(self::$_instance)) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}

		if (!isset(self::$_instance->_cache)) {
			self::$_instance->_cache = array();
		}

		return self::$_instance;
	}

	public static function retrieve_object($type, $id) {
		$i = self::getInstance();
		if (!isset($i->_cache[$type])) {
			$i->_cache[$type] = array();
		}
		$class = "Cygol3\\".$type;
		if (!isset($i->_cache[$type][$id])) {
			return ObjectCache::refresh_object($type, $id);
		}
		return $i->_cache[$type][$id];
	}

	public static function refresh_object($type, $id) {
		$i = self::getInstance();
		if (!isset($i->_cache[$type])) {
			$i->_cache[$type] = array();
		}
		$class = "Cygol3\\".$type;
		$i->_cache[$type][$id] = new $class($id);
		return $i->_cache[$type][$id];
	}
}

class Cygol3 {
	static $config = NULL;
	static $current_group = NULL;
	static $logged_user = NULL;
	static $current_actor = NULL;
	static $lang = NULL;
	static $locales_by_lang = NULL;
	static $locales_by_region = NULL;
	static $db = NULL;
	static $_db_transaction_level = 0;
	static $view_params = array();
	static $cookies = array();
}
