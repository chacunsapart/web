<?php

namespace Cygol3;

require('/usr/share/php/smarty3/Smarty.class.php');

function get_smarty_object() {
	$smarty = new \Smarty;
	$smarty->setCompileDir('../cache/compiled-templates');

	return $smarty;
}

$smarty = get_smarty_object();
$smarty->registerPlugin("modifier","format_amount", "\Cygol3\\format_amount");
$smarty->registerPlugin("modifier","format_amount_form", "\Cygol3\\format_amount_form");
$smarty->registerPlugin("modifier","format_part", "\Cygol3\\format_part");
$smarty->registerPlugin("modifier","format_fraction", "\Cygol3\\format_fraction");
$smarty->registerPlugin("modifier","format_locale", "\Cygol3\\format_locale");
$smarty->registerPlugin("modifier","format_locale_getlang", "\Cygol3\\format_locale_getlang");
$smarty->registerPlugin("modifier","format_locale_getregion", "\Cygol3\\format_locale_getregion");
$smarty->registerPlugin("modifier","my_encode_b64", "\Cygol3\\my_encode_b64");
$smarty->registerPlugin("modifier","my_decode_b64", "\Cygol3\\my_decode_b64");


function set_feedback($message, $level) {
	global $smarty;

	$smarty->assign('feedback', $message);
	$smarty->assign('feedback_level', "alert-".$level);
}
set_feedback('', '');
