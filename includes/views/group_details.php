<?php

namespace Cygol3;

if (!isset($active_tab)) {
	$active_tab = 'expenses';
}
$smarty->assign("active_tab", $active_tab);
if (!isset($show_all)) {
	$show_all = 0;
}
$smarty->assign("show_all", $show_all);
$smarty->assign("now", time());

$smarty->assign("group", $e);
setup_current_group($e);

$expenses = $e->get_expenses();
$smarty->assign("expenses", $expenses);

$participations = array();
foreach ($expenses as $expense) {
	$participations[$expense->id] = $expense->get_participations();
	usort($participations[$expense->id], "Cygol3\cmp_participations_by_nick");
}
$smarty->assign("participations", $participations);

$balances = $e->get_balances();
$smarty->assign("balances", $balances);

$suggestions = $e->get_suggestions();
$smarty->assign("suggestions", $suggestions);

$smarty->assign("group_cookie", $e->invitation_cookie);
$gc = get_group_cookies();
if (count($gc) && $gc[0] == $e->invitation_cookie) {
	$nc = $gc;
} else {
	$nc = array();
	$nc[] = $e->invitation_cookie;
	foreach ($gc as $c) {
		if ($c != $e->invitation_cookie) {
			$nc[] = $c;
		}
	}
}
$nc = array_slice($nc, 0, 3);
$newcookie = implode('/', $nc);
set_cookie("group_cookie", $newcookie);

$potential_payers = get_potential_payers($e);

$smarty->assign("potential_payers", $potential_payers);

$smarty->assign("date", date('Y-m-d'));

$smarty->display("../templates/group_details.tpl");
