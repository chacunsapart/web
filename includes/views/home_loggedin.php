<?php

namespace Cygol3;

$g1 = Cygol3::$logged_user->get_groups();
$g2 = Group::get_groups_from_cookies();
$groups = merge_group_list($g1, $g2);
$smarty->assign("groups", $groups);

$groups_participants = array();
foreach ($groups as $g) {
	$row = array('group' => $g,
		     'participants' => $g->get_participants(),
		     'expenses' => $g->get_expenses());
	$groups_participants[] = $row;
}
$smarty->assign("groups_participants", $groups_participants);

$smarty->assign("show_all", getIntFromRequest('all'));
$smarty->assign("now", time());

$smarty->display("../templates/home_loggedin.tpl");
