<?php

namespace Cygol3;

$smarty->assign('logged_user', Cygol3::$logged_user);

if (!isset($active_tab)) {
	$active_tab = 'general';
}
$smarty->assign("active_tab", $active_tab);

$smarty->display("../templates/my_account.tpl");
