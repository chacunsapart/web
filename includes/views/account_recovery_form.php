<?php

namespace Cygol3;

$smarty->assign("email", $email);
$smarty->assign("recoverycode", $recoverycode);

$smarty->display("../templates/account_recovery_form.tpl");
