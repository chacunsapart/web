<?php

namespace Cygol3;

$smarty->assign("expense", $expense);
$participations = $expense->get_participations();
setup_current_group($expense->group);

$expense->load($expense->id); // Rechargement pour calcul des montants avec la bonne conversion

$total_parts = 0;
foreach ($participations as $p) {
	$total_parts += $p->parts;
}
$smarty->assign("total_parts", $total_parts);
if ($total_parts > 0) {
	$part_value = ($expense->amount * $expense->group->fraction) / $total_parts;
	$smarty->assign("part_value", $part_value);
}

if (!isset($active_tab)) {
	$active_tab = 'guests';
}
$smarty->assign("active_tab", $active_tab);

$participants = array();
foreach ($participations as $px) {
	$participants[] = array('guest' => $px->guest,
				'parts' => $px->parts,
				'px_id' => $px->id);
}

$seen = array();

$current_expense_participants = array();
foreach($participations as $participation) {
	if (!isset($seen[$participation->guest->id])) {
		$current_expense_participants[] = $participation->guest;
		$seen[$participation->guest->id] = true;
	}
	$seen[$participation->guest->id] = true;
}

$other_group_participants = array();
foreach ($expense->group->get_participants() as $eg) {
	if (!isset($seen[$eg->id])) {
		$other_group_participants[] = $eg;
		$participants[] = array('guest' => $eg,
					'parts' => 0);
		$seen[$eg->id] = true;
	}
}

$other_friends = array();
if (Cygol3::$logged_user) {
	if (!isset($seen[Cygol3::$current_actor->id])) {
		$other_friends[] = Cygol3::$current_actor;
		$seen[Cygol3::$current_actor->id] = true;
	}
}

foreach (get_all_friends() as $f) {
	if (!isset($seen[$f->id])) {
		$other_friends[] = $f;
		$seen[$f->id] = true;
	}
}

// usort($other_friends, "Cygol3\cmp_actors_by_nick");
$smarty->assign("other_friends", $other_friends);

$potential_payers = array_merge($current_expense_participants,
				$other_group_participants,
				$other_friends);

$smarty->assign("potential_payers", $potential_payers);

usort($participants, "Cygol3\cmp_participants_by_nick");
$smarty->assign("participants", $participants);

$smarty->display("../templates/expense_details.tpl");
