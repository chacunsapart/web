<?php

namespace Cygol3;

$groups = Group::get_groups_from_cookies();
$smarty->assign("groups", $groups);

$groups_participants = array();
foreach ($groups as $g) {
	$row = array('group' => $g,
		     'participants' => $g->get_participants(),
		     'expenses' => $g->get_expenses());
	$groups_participants[] = $row;
}
$smarty->assign("groups_participants", $groups_participants);

$smarty->display("../templates/home_notloggedin.tpl");
