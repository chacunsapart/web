<?php

namespace Cygol3;

$QR_BASEDIR = '/usr/share/phpqrcode/';

include $QR_BASEDIR."qrconst.php";
// include $QR_BASEDIR."qrconfig.php";
define('QR_CACHEABLE', true);
define('QR_CACHE_DIR', '../cache/qr-lib/');
define('QR_LOG_DIR', false);
define('QR_FIND_BEST_MASK', true);
define('QR_FIND_FROM_RANDOM', false);
define('QR_DEFAULT_MASK', 2);
define('QR_PNG_MAXIMUM_SIZE',  1024);

include $QR_BASEDIR."qrtools.php";
include $QR_BASEDIR."qrspec.php";
include $QR_BASEDIR."qrimage.php";
include $QR_BASEDIR."qrinput.php";
include $QR_BASEDIR."qrbitstream.php";
include $QR_BASEDIR."qrsplit.php";
include $QR_BASEDIR."qrrscode.php";
include $QR_BASEDIR."qrmask.php";
include $QR_BASEDIR."qrencode.php";

if ($p == 'home') {
	$qrdata = '';
} else {
	$qrdata = my_decode_b64($p);
}

$cacheprefix = "../root/qr";
if ($qrdata) {
	$url = Cygol3::$config['prefix'].$qrdata;
	$fname = $p;
	$fname = preg_replace('/(.*)(.)(.)(....)$/', '$2/$3/$1$2$3$4', $fname);
	$subdir = preg_replace('/(.*)(.)(.)(....)$/', '$2/$3', $fname);
	$dir = $cacheprefix.'/'.$subdir;
	if (! is_dir($dir)) {
		mkdir ($dir, 0755, true);
	}
} else {
	$url = Cygol3::$config['prefix'];
	$fname = 'home';
}

\QRcode::png($url, "$cacheprefix/$fname.png");
\QRcode::png($url);
